gitBranch = $(shell git rev-parse --abbrev-ref HEAD)
gitTime = $(shell date +'%FT%T%:z')

#### cmd
build:
	echo ">>> git branch: ${gitBranch}, git time: ${gitTime}"
	bash scripts/go_build.sh go-web
	ls -lh target/go-web

build-vendor:
	scripts/build.vendor.sh dev

run:
	bash scripts/go_build.sh go-web
	./target/go-web

test-release:
	go build -o target/main main.go
	./target/main --release

#### csfm
build-csfm:
	mkdir -p target
	GOOS=linux   GOARCH=amd64 go build -o target/csfm     -ldflags="-w -s" bin/csfm/csfm.go
	GOOS=windows GOARCH=amd64 go build -o target/csfm.exe -ldflags="-w -s" bin/csfm/csfm.go
	ls -lh target/csfm target/csfm.exe

api-test:
	mkdir -p target
	GOOS=linux GOARCH=amd64 go build -o target/api-test bin/api-test/api-test.go
	ls -lh target/api-test

#### greet_grpc
gen:
	protoc --go_out=./  --go-grpc_out=./  pkg/greet/greet.proto
	sed -i '/^\tmustEmbedUnimplemented/s#\t#\t// #' pkg/greet/greet_grpc.pb.go

clean:
	rm -f data/grpc_send.data data/grpc_recevied.data
	truncate -s 10001k data/grpc_send.data

greet-server:
	go run bin/greet_grpc/*.go server

greet-client:
	go run bin/greet_grpc/*.go client

test:
	go test -cover -race ./...

.PHONY: gen, clean, grpc-server, grpc-client
