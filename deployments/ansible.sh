#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})


[[ -f "hosts.ini" ]] || { echo "missing hosts.ini"; exit 1; }

app_env=$1

# ~/.ansible.cfg
# [defaults]
# inventory = ~/.ansible/hosts.ini
# log_path = $PWD/ansible.log

# --become: root user
time ansible-playbook -vv --inventory=hosts.ini \
  "${_path}/ansible.playbook.yaml" --extra-vars="app_env=$app_env"
