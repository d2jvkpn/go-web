#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})


function on_exit {
    git checkout dev # --force
    rm -f build.lock
}
mkfifo build.lock
trap on_exit EXIT
build_vendor=$(printenv BUILD_Vendor || true)

gitBranch=$1
name="registry.cn-shanghai.aliyuncs.com/d2jvkpn/go-web"
tag=$gitBranch

#### git
b1="$(git rev-parse --abbrev-ref HEAD)" # current branch
uncommitted=$(git status --short)
unpushed=$(git diff origin/$b1..HEAD --name-status)

if [[ "$build_vendor" != "true" ]]; then
    test -z "$uncommitted" || { echo "You have uncommitted changes!"; exit 1; }
    test -z "$unpushed" || { echo "You have unpushed commits!"; exit 1; }
fi

git checkout $gitBranch # --force
if [[ "$build_vendor" != "true" ]]; then
    git pull --no-edit
else
    go mod vendor
fi

#### build
buildTime=$(date +'%FT%T%:z')
gitCommit=$(git rev-parse --verify HEAD) # git log --pretty=format:'%h' -n 1
gitTime=$(git log -1 --format="%at" | xargs -I{} date -d @{} +%FT%T%:z)
# git tag $git_tag
# git push origin $git_tag
gitTreeState="clean"

uncommitted=$(git status --short)
unpushed=$(git diff origin/$gitBranch..HEAD --name-status)
[ ! -z "$uncommitted$unpushed" ] && gitTreeState="dirty"

#### build image
if [[ "$build_vendor" != "true" ]]; then
    echo ">>> pull images..."
    for base in $(awk '/^FROM/{print $2}' ${_path}/Dockerfile); do
        docker pull --quiet $base
        bn=$(echo $base | awk -F ":" '{print $1}')
        if [[ -z "$bn" ]]; then continue; fi
        docker images --filter "dangling=true" --quiet "$bn" | xargs -i docker rmi {}
    done &> /dev/null
fi

# prev=$(docker images $name:$tag -q)
echo ">>> build image: $name:$tag..."

GO_ldflags="-X main.buildTime=$buildTime -X main.gitBranch=$gitBranch \
  -X main.gitCommit=$gitCommit -X main.gitTime=$gitTime \
  -X main.gitTreeState=$gitTreeState"

df=${_path}/Dockerfile.vendor
[ "$build_vendor" != "true" ] && df=${_path}/Dockerfile

docker build --no-cache --file $df --build-arg=GO_ldflags="$GO_ldflags" --tag $name:$tag ./
docker image prune --force --filter label=stage=go-web_builder &> /dev/null

#### push
echo ">>> push image: $name:$tag..."
docker push $name:$tag

for img in $(docker images --filter "dangling=true" --quiet $name); do
    docker rmi $img || true
done &> /dev/null
