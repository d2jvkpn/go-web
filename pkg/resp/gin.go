package resp

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func BindJSON[J any, R any](fn func(context.Context, J) (R, *Error)) gin.HandlerFunc {
	// data := gin.H{"code":-1,"msg":"failed to parse body","data":gin.H{}}
	bts := []byte(`{"code":-1,"msg":"failed to parse json","data":{}}`)
	length := fmt.Sprintf("%d", len(bts))

	return func(ctx *gin.Context) {
		var (
			err  error
			item J
		)

		if err = ctx.BindJSON(&item); err != nil {
			// ctx.JSON(http.StatusBadRequest, data)
			ctx.Header("Content-Type", "application/json")
			ctx.Header("Content-Length", length)
			ctx.Writer.WriteHeader(http.StatusBadRequest)
			_, _ = ctx.Writer.Write(bts)
			ctx.Abort()
			return
		}

		if result, err := fn(ctx, item); err != nil {
			JSON(ctx, nil, err)
		} else {
			JSON(ctx, result, nil)
		}
	}
}

func BindQuery[Q any, R any](fn func(context.Context, Q) (R, *Error)) gin.HandlerFunc {
	// data := gin.H{"code":-1,"msg":"failed to parse query","data":gin.H{}}
	bts := []byte(`{"code":-1,"msg":"failed to parse query","data":{}}`)
	length := fmt.Sprintf("%d", len(bts))

	return func(ctx *gin.Context) {
		var (
			err  error
			item Q
		)

		if err = ctx.BindQuery(&item); err != nil {
			// ctx.JSON(http.StatusBadRequest, data)
			ctx.Header("Content-Type", "application/json")
			ctx.Header("Content-Length", length)
			ctx.Writer.WriteHeader(http.StatusBadRequest)
			_, _ = ctx.Writer.Write(bts)
			ctx.Abort()
			return
		}

		if result, err := fn(ctx, item); err != nil {
			JSON(ctx, nil, err)
		} else {
			JSON(ctx, result, nil)
		}
	}
}
