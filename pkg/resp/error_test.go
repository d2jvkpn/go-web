package resp

import (
	"fmt"
	"testing"
)

func TestError_t1(t *testing.T) {
	fmt.Printf("%v\n", ErrNotRoute())
	fmt.Printf("%v\n", ErrParseFailed(testErr))
	fmt.Printf("%v\n", ErrBadRequest(testErr, ""))
	fmt.Printf("%v\n", ErrInvalidParameter(testErr, testMsg))

	fmt.Printf("%v\n", ErrInvalidToken(testErr))
	fmt.Printf("%v\n", ErrUnauthorized(testErr, ""))
	fmt.Printf("%v\n", ErrLoginRequired())
	fmt.Printf("%v\n", ErrConflict(testErr, testMsg))
	fmt.Printf("%v\n", ErrNotFound(testErr))
	fmt.Printf("%v\n", ErrAccessDenied(testErr, ""))

}

//func TestError_t2(t *testing.T) {
//	var err error
//	err = ErrParseFailed(testErr)
//	fmt.Println(err)
//}

//func TestError_t3(t *testing.T) {
//	var (
//		e1  *Error
//		err error
//	)

//	err = e1
//	fmt.Println(e1 == nil, err == nil) // true, false
//}

func t_caller01() *Error {
	return ErrNotRoute()
}

func t_caller02() *Error {
	return t_caller01()
}

func TestErrror_t3(t *testing.T) {
	fmt.Printf(
		">>> t_caller01: %v\n    t_caller02: %v\n    t_caller02 and update: %v\n",
		t_caller01(), t_caller02(),
		t_caller02().Update(),
	)
}
