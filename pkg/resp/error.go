package resp

import (
	// "errors"
	"fmt"
	"net/http"
	"path/filepath"
	"runtime"
)

type Error struct {
	Cause    string `json:"cause"`
	HttpCode int    `json:"httpCode"`
	Code     int    `json:"code"`
	Msg      string `json:"msg"`

	cause error
	skip  int
}

type Option func(*Error) bool

func Msg(msg string) Option {
	return func(e *Error) bool {
		e.Msg = msg
		return true
	}
}

//func (out Error) String() string {
//	return fmt.Sprintf(
//		"httpCode: %d, code: %d, msg: %s, cause: %s",
//		out.HttpCode, out.Code, out.Msg, out.Cause,
//	)
//}

func Skip(skip int) Option {
	return func(e *Error) bool {
		if skip > 1 {
			e.skip = skip
			return true
		}
		return false
	}
}

func NewError(cause error, httpCode, code int, opts ...Option) (out *Error) {
	if cause == nil { // avoid panic
		return nil
	}

	// httpCode != http.StatusOK, code != 0
	out = &Error{HttpCode: httpCode, Code: code, Msg: "", skip: 1, cause: cause}
	for _, v := range opts {
		_ = v(out)
	}

	fn, file, line, _ := runtime.Caller(out.skip)
	out.Cause = fmt.Sprintf(
		"%s(%s:%d): %v", runtime.FuncForPC(fn).Name(), filepath.Base(file), line, cause,
	)

	return out
}

func (err *Error) Update() *Error {
	err.skip -= 1

	fn, file, line, _ := runtime.Caller(err.skip)
	err.Cause = fmt.Sprintf(
		"%s(%s:%d): %v", runtime.FuncForPC(fn).Name(), filepath.Base(file), line, err.cause,
	)

	return err
}

// don't impls this
//func (out *Error) Error() string {
//	return out.Cause
//}

func (out *Error) GetCause() error {
	// return errors.Unwrap(err.cause)
	return out.cause
}

/*
- route: match, check authority
- kv in context: _UserId, Biz, error/event
- parse data

- valid data
- precondition: ...

- process: ...
- return valulue

code ranges:

	...=-100 has no right
	-99=-1   invalid request
	0        ok
	1..=99   business error
	100...   unexpected error
*/

// ..-1
func ErrNotRoute() (out *Error) {
	msg := "no route"
	return NewError(fmt.Errorf(msg), http.StatusNotFound, -1, Skip(2), Msg(msg))
}

func ErrParseFailed(cause error) (out *Error) {
	return NewError(cause, http.StatusBadRequest, -2, Skip(2), Msg("parse failed"))
}

func ErrBadRequest(cause error, msg string) (out *Error) {
	if msg == "" {
		msg = "bad request"
	} else {
		msg = fmt.Sprintf("bad request: %s", msg)
	}
	return NewError(cause, http.StatusBadRequest, -3, Skip(2), Msg(msg))
}

func ErrInvalidParameter(cause error, msg string) (out *Error) {
	if msg == "" {
		msg = "invalid parameter"
	} else {
		msg = fmt.Sprintf("invalid parameter: %s", msg)
	}
	return NewError(cause, http.StatusBadRequest, -4, Skip(2), Msg(msg))
}

// ..-100
func ErrInvalidToken(cause error) (out *Error) {
	return NewError(cause, http.StatusUnauthorized, -101, Skip(2), Msg("invalid token"))
}

func ErrUnauthorized(cause error, msg string) (out *Error) {
	if msg == "" {
		msg = "unauthorized"
	} else {
		msg = fmt.Sprintf("unauthorized: %s", msg)
	}
	return NewError(cause, http.StatusUnauthorized, -102, Skip(2), Msg(msg))
}

func ErrLoginRequired() (out *Error) {
	msg := "login required"
	return NewError(fmt.Errorf(msg), http.StatusSeeOther, -103, Skip(2), Msg(msg))
}

func ErrConflict(cause error, msg string) (out *Error) {
	if msg == "" {
		msg = "conflict"
	} else {
		msg = fmt.Sprintf("conflict: %s", msg)
	}
	return NewError(cause, http.StatusConflict, -104, Skip(2), Msg(msg))
}

func ErrNotFound(cause error) (out *Error) {
	return NewError(cause, http.StatusNotFound, -105, Skip(2), Msg("not found"))
}

func ErrAccessDenied(cause error, msg string) (out *Error) {
	if msg == "" {
		msg = "access denied"
	} else {
		msg = fmt.Sprintf("access denied: %s", msg)
	}
	return NewError(cause, http.StatusForbidden, -106, Skip(2), Msg(msg))
}

func ErrNoResource(cause error) (out *Error) {
	return NewError(cause, http.StatusNotFound, -107, Skip(2), Msg("not resource"))
}

// 100..
func ErrNotImplemented(cause error) (out *Error) {
	return NewError(cause, http.StatusNotImplemented, 100, Skip(2), Msg("not implemented"))
}

func ErrUnexpected(cause error) (out *Error) {
	return NewError(cause, http.StatusInternalServerError, 101, Skip(2), Msg("unepected"))
}

func ErrServerError(cause error) (out *Error) {
	return NewError(
		cause, http.StatusInternalServerError, 102, Skip(2), Msg("internal server error"),
	)
}

func ErrPanic(data any, opts ...Option) (out *Error) {
	opts2 := make([]Option, 0, len(opts)+2)
	opts2 = append(opts2, Skip(2))
	opts2 = append(opts2, opts...)
	opts2 = append(opts2, Msg("internal server error"))

	return NewError(
		fmt.Errorf("%v", data),
		http.StatusInternalServerError, 103,
		opts2...,
	)
}

func ErrServerIsBusy(casue error) (out *Error) {
	return NewError(casue, http.StatusServiceUnavailable, 104, Skip(2), Msg("service is busy"))
}

func ErrTooManyRequests(casue error) (out *Error) {
	return NewError(casue, http.StatusTooManyRequests, 105, Skip(2), Msg("too many requests"))
}

func ErrThirdPartyService(casue error, msg string) (out *Error) {
	msg = fmt.Sprintf("third party service error: %s", msg)
	return NewError(casue, http.StatusFailedDependency, 106, Skip(2), Msg(msg))
}
