package resp

import (
// "fmt"
)

const (
	KEY_RequestId = "_RequestId"
	KEY_UserId    = "_UserId"
	KEY_UserRole  = "UserRole"
	KEY_User      = "User"
	KEY_Error     = "error"
	KEY_Event     = "event"
)
