/*
#### Packages
  - github.com/spf13/viper
  - github.com/stretchr/testify
  - github.com/hashicorp/consul/api
  - google.golang.org/grpc
  - github.com/gin-gonic/gin
  - gopkg.in/gomail.v2
  - github.com/golang-jwt/jwt/v4
  - go.uber.org/multierr
  - go.uber.org/zap
  - gopkg.in/natefinch/lumberjack.v2
*/
package wrap
