package wrap

import (
	"flag"
	"fmt"
	"log"
	"net/url"
	"testing"

	"github.com/hashicorp/consul/api"
)

var (
	_Key    string
	_Client *api.Client
)

func TestUrl(t *testing.T) {
	u, err := url.Parse("http://127.0.0.1:8081")
	if err != nil {
		t.Fatal(err)
	}

	fmt.Printf("%#v\n", u)
}

func TestMain(m *testing.M) {
	var (
		err    error
		config *api.Config
	)

	flag.Parse()

	config = api.DefaultConfig()
	config.Address, _Key = "http://127.0.0.1:8500", "my-service"
	config.Token = "7a1798a3-4e9b-d98d-6186-59bbddd1f851" // k/v: mys-service, policy: mys-service

	args := flag.Args()
	if len(args) > 0 {
		config.Address = args[0]
	}
	if len(args) > 1 {
		_Key = args[1]
	}

	if _Client, err = api.NewClient(config); err != nil {
		log.Fatalln(err)
	}

	m.Run()
}

// $ go test -run TestConsulClient -- http://127.0.0.1:8500
func TestConsulDiscovery_t1(t *testing.T) {
	var (
		err    error
		data   map[string]*api.AgentService
		checks []api.AgentServiceChecksInfo
	)

	data, err = _Client.Agent().ServicesWithFilter(fmt.Sprintf(`Service == "%s"`, _Key))
	if err != nil {
		t.Fatal(err)
	}

	for k, v := range data {
		// fmt.Printf(">>> %#v\n", v)
		fmt.Printf(
			"~~~ key=%q, address=%q, port=%d, ns=%q, kind=%q\n",
			k, v.Address, v.Port, v.Namespace, v.Kind,
		)
	}

	_, checks, err = _Client.Agent().AgentHealthServiceByNameOpts(_Key, nil)
	if err != nil {
		t.Fatal(err)
	}

	for _, v := range checks {
		fmt.Printf(">>> %s, %#v\n", v.AggregatedStatus, v.Service) // critical, passing
	}
}

func TestConsulDiscovery_t2(t *testing.T) {
	var (
		err    error
		data   map[string]*api.AgentService
		checks []api.AgentServiceChecksInfo
	)

	service := "greet"

	data, err = _Client.Agent().ServicesWithFilter(fmt.Sprintf(`Service == "%s"`, service))
	if err != nil {
		t.Fatal(err)
	}

	for k, v := range data {
		// fmt.Printf(">>> %#v\n", v)
		fmt.Printf(
			"~~~ key=%q, address=%q, port=%d, ns=%q, kind=%q\n",
			k, v.Address, v.Port, v.Namespace, v.Kind,
		)
	}

	_, checks, err = _Client.Agent().AgentHealthServiceByNameOpts(service, nil)
	if err != nil {
		t.Fatal(err)
	}

	for _, v := range checks {
		fmt.Printf(">>> %s, %#v\n", v.AggregatedStatus, v.Service) // critical, passing
	}
}

func TestConsulKV(t *testing.T) {
	pair, meta, err := _Client.KV().Get(_Key, nil)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Printf("~~~ KVPair: %#v\n    QueryMeta: %#v\n", pair, meta)

	if pair != nil {
		fmt.Printf("    key=%q, value=%q\n", pair.Key, pair.Value)
	}
}
