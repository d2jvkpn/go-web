package wrap

import (
	// "fmt"
	"os"
	"testing"
)

func TestLogger(t *testing.T) {
	logger := NewLogger("logs/test.log", LogLevelFromStr("debug"), 256, os.Stdout)

	logger.Info("OK")
	logger.Error("Wrong")
	logger.Down()
}
