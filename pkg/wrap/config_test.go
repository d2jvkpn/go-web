package wrap

import (
	"testing"

	"github.com/spf13/viper"
	. "github.com/stretchr/testify/require"
)

func TestConfig(t *testing.T) {
	type Github struct {
		Name string
		Link string
	}

	type Gitlab struct {
		User    string `mapstructure:"user"`
		Address string `mapstructure:"address"`
	}

	github, gitlab := new(Github), new(Gitlab)

	var (
		user   string
		err    error
		config *viper.Viper
	)

	err = UnmarshalConfig("config.yaml", map[string]any{
		"github": github,
		"gitlab": gitlab,
	})
	NoError(t, err)

	Equal(t, "d2jvkpn", github.Name)
	Equal(t, "https://github.com/d2jvkpn", github.Link)

	Equal(t, "d2jvkpn", gitlab.User)
	Equal(t, "https://gitlab.com/d2jvkpn", gitlab.Address)

	user, err = ReadStringField("config.yaml", "gitlab.user")
	NoError(t, err)
	Equal(t, "d2jvkpn", user)

	config, err = ConfigFromBytes([]byte("a: 1"), "yaml")
	NoError(t, err)
	Equal(t, 1, config.GetInt("a"))
}
