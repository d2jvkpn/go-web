package wrap

import (
	"bytes"
	"fmt"

	"github.com/spf13/viper"
)

func OpenConfig(fp string, types ...string) (conf *viper.Viper, err error) {
	conf = viper.New()
	// conf.SetConfigName(name)
	if len(types) > 0 {
		conf.SetConfigType(types[0])
	} else {
		conf.SetConfigType("yaml")
	}

	conf.SetConfigFile(fp)
	if err = conf.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("ReadInConfig(): %q, %v", fp, err)
	}

	return conf, nil
}

func UnmarshalConfig(fp string, objects map[string]any) (err error) {
	var conf *viper.Viper

	if conf, err = OpenConfig(fp); err != nil {
		return err
	}

	for k, v := range objects {
		if err = conf.UnmarshalKey(k, v); err != nil {
			return err
		}
	}

	return nil
}

func ConfigFromBytes(bts []byte, typ string) (conf *viper.Viper, err error) {
	buf := bytes.NewBuffer(bts)

	conf = viper.New()
	// conf.SetConfigName(name)
	conf.SetConfigType(typ)
	if err = conf.ReadConfig(buf); err != nil {
		return nil, fmt.Errorf("ReadConfig(): %v", err)
	}

	return conf, nil
}

func ReadStringField(fp, sp string) (out string, err error) {
	var conf *viper.Viper

	if conf, err = OpenConfig(fp); err != nil {
		return "", err
	}

	return conf.GetString(sp), nil
}
