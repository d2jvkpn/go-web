package orm

import (
	"fmt"
	"time"

	. "github.com/d2jvkpn/go-web/pkg/resp"

	"gorm.io/gorm"
)

type QueryDate struct {
	DateStart  string `json:"dateStart,omitempty" form:"dateStart"`
	DateEnd    string `json:"dateEnd,omitempty" form:"dateEnd"`
	MonthStart string `json:"monthStart,omitempty" form:"monthStart"`
	MonthEnd   string `json:"monthEnd,omitempty" form:"monthEnd"`
}

func (query *QueryDate) Validate() (err *Error) {
	if query.DateStart != "" {
		if _, e := time.Parse(time.DateOnly, query.DateStart); e != nil {
			msg := "invalid dateStart"
			return ErrInvalidParameter(fmt.Errorf(msg), msg)
		}
	}

	if query.DateEnd != "" {
		if _, e := time.Parse(time.DateOnly, query.DateEnd); e != nil {
			msg := "invalid dateEnd"
			return ErrInvalidParameter(fmt.Errorf(msg), msg)
		}
	}

	if query.MonthStart != "" {
		if _, e := time.Parse("2006-01", query.MonthStart); e != nil {
			msg := "invalid monthStart"
			return ErrInvalidParameter(fmt.Errorf(msg), msg)
		}
	}

	if query.MonthEnd != "" {
		if _, e := time.Parse("2006-01", query.MonthEnd); e != nil {
			msg := "invalid monthEnd"
			return ErrInvalidParameter(fmt.Errorf(msg), msg)
		}
	}

	if query.DateStart == "" && query.MonthStart == "" {
		msg := "both dateStart and monthStart are empty"
		return ErrInvalidParameter(fmt.Errorf(msg), msg)
	}

	if query.DateStart != "" && query.DateEnd == "" {
		query.DateEnd = query.DateStart
	}

	if query.MonthStart != "" && query.MonthEnd == "" {
		query.MonthStart = query.MonthEnd
	}

	return nil
}

func (query *QueryDate) Do(db *gorm.DB, field string) (*gorm.DB, *Error) {
	if err := query.Validate(); err != nil {
		return nil, err
	}

	if query.DateStart != "" {
		return query.dateRange(db, field), nil
	} else {
		return query.monthRange(db, field), nil
	}
}

func (query *QueryDate) dateRange(db *gorm.DB, field string) *gorm.DB {
	// DATE(xx) AS yy
	state := fmt.Sprintf(`DATE(%[1]s) >= ? AND DATE(%[1]s) <= ?`, field)
	return db.Where(state, query.DateStart, query.DateEnd)
}

func (query *QueryDate) monthRange(db *gorm.DB, field string) *gorm.DB {
	// EXTRACT(YEAR_MONTH FROM xx) AS yy
	state := fmt.Sprintf(`DATE(%[1]s) >= ? AND DATE(%[1]s) < DATE_ADD(?, INTERVAL 1 MONTH)`, field)
	return db.Where(state, query.MonthStart+"-01", query.MonthEnd+"-01")
}
