package orm

import (
	"fmt"

	"gorm.io/gorm"
)

// query
type Query struct {
	Starting string `json:"starting,omitempty" form:"starting"` // 2022-10-28
	End      string `json:"end,omitempty" form:"end"`           // 2022-10-28
	PageNo   int    `json:"pageNo,omitempty" form:"pageNo"`
	PageSize int    `json:"pageSize,omitempty" form:"pageSize"`
	Search   string `json:"search,omitempty" form:"search"`
	Status   string `json:"status,omitempty" form:"status"`
	UserId   int64  `json:"-" form:"-"`
}

func (query *Query) Validate() (err error) {
	if query.PageNo < 0 || query.PageSize < 0 {
		msg := "invalid pageNo or pageSize"
		return fmt.Errorf(msg)
	}

	if query.Starting != "" && !_DateRE.MatchString(query.Starting) {
		msg := "invalid starting"
		return fmt.Errorf(msg)
	}

	if query.End != "" && !_DateRE.MatchString(query.End) {
		msg := "invalid end"
		return fmt.Errorf(msg)
	}

	if query.PageNo == 0 {
		query.PageNo = 1
	}

	if query.PageSize == 0 {
		query.PageSize = PageSizeMin
	} else if query.PageSize > PageSizeMax {
		query.PageSize = PageSizeMax
	}

	return nil
}

func (query *Query) DateRange(tx *gorm.DB, field string) *gorm.DB {
	if query.Starting != "" {
		tx = tx.Where(field+" >= ?", query.Starting)
	}
	if query.End != "" {
		tx = tx.Where(field+" < DATE_ADD(?, INTERVAL 1 DAY)", query.End)
	}
	return tx
}

func (query *Query) Flip(tx *gorm.DB) *gorm.DB {
	return tx.Limit(query.PageSize).Offset((query.PageNo - 1) * query.PageSize)
}
