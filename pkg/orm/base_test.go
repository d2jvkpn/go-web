package orm

import (
	"context"
	"flag"
	"fmt"
	"os"
	"testing"

	"github.com/d2jvkpn/go-web/pkg/misc"
)

var (
	testFlag *flag.FlagSet   = nil
	testCtx  context.Context = context.Background()
)

func TestMain(m *testing.M) {
	var (
		configFile string
		err        error
	)

	testFlag = flag.NewFlagSet("testFlag", flag.ExitOnError)
	flag.Parse() // must do

	testFlag.StringVar(&configFile, "config", "configs/local.yaml", "config filepath")

	testFlag.Parse(flag.Args())
	fmt.Printf("~~~ load config %s\n", configFile)

	defer func() {
		if err != nil {
			fmt.Printf("!!! TestMain: %v\n", err)
			os.Exit(1)
		}
	}()

	if configFile, err = misc.RootFile(configFile); err != nil {
		return
	}

	if _DB, err = Connect(configFile, "mysql.dsn", true); err != nil {
		return
	}

	m.Run()
}
