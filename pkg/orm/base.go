package orm

import (
	"fmt"
	"regexp"
	"sync"

	"gorm.io/gorm"
)

var (
	_DB   *gorm.DB   = nil
	_Once *sync.Once = new(sync.Once)
)

func InitDB(db *gorm.DB) (err error) {
	if db == nil {
		return fmt.Errorf("db is nil")
	}

	_Once.Do(func() { _DB = db })
	return nil
}

const (
	PageSizeMin = 10
	PageSizeMax = 50
)

var (
	_DateRE = regexp.MustCompile(`^\d{4}-\d{2}-\d{2}$`)
)
