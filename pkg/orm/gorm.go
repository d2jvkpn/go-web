package orm

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"database/sql/driver"
	go_mysql "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

/*
MySQL initialize

	dsn format: {USERANME}:{PASSWORD}@tcp({IP})/{DATABASE}?charset=utf8mb4&parseTime=True&loc=Local
*/
func ConnectDSN(dsn string, debugMode bool) (db *gorm.DB, err error) {
	conf := &gorm.Config{
		NamingStrategy: schema.NamingStrategy{SingularTable: true},
	}

	if _DB, err = gorm.Open(mysql.Open(dsn), conf); err != nil {
		return nil, err
	}
	if debugMode {
		_DB = _DB.Debug()
	}
	/*
		sqlDB, err := db.DB() // Get generic database object sql.DB to use its functions
		sqlDB.SetMaxIdleConns(10)
		sqlDB.SetMaxOpenConns(100)
		sqlDB.SetConnMaxLifetime(time.Hour)
	*/

	return _DB, err
}

// connect to database by provide a config file path(under project directory), and then use field mysql.dsn
func Connect(fp, field string, debugMode bool) (db *gorm.DB, err error) {
	conf := viper.New()
	conf.SetConfigName("mysql config")
	conf.SetConfigFile(fp)

	if err = conf.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("ReadInConfig(): %w", err)
	}

	dsn := conf.GetString(field)
	if db, err = ConnectDSN(dsn, debugMode); err != nil {
		return nil, fmt.Errorf("Connect: %w", err)
	}

	return db, nil
}

// errors
func IsNotFound(err error) bool {
	return errors.Is(err, gorm.ErrRecordNotFound)
}

func IsDuplicateEntry(err error) bool {
	sqlErr, ok := err.(*go_mysql.MySQLError)
	return ok && sqlErr.Number == uint16(1062)
}

func Like(tx *gorm.DB, field, val string) *gorm.DB {
	if field == "" || val == "" {
		return tx
	}
	return tx.Where(field+" LIKE ?", "%"+val+"%")
}

func Where(tx *gorm.DB, field, val string) *gorm.DB {
	if field == "" || val == "" {
		return tx
	}
	return tx.Where(field+" = ?", val)
}

func MultiEquals[T any](tx *gorm.DB, field string, values []T) *gorm.DB {
	if len(values) == 0 {
		return tx
	}

	wheres := make([]string, len(values))
	intfs := make([]any, len(values))

	for i := range values {
		wheres[i] = fmt.Sprintf("%s = ?", field)
		intfs[i] = values[i]
	}

	tx = tx.Where(strings.Join(wheres, " OR "), intfs...)
	return tx
}

// result
type Result[T any] struct {
	Total int64 `json:"total"`
	Items []T   `json:"items"`
}

func NewResult[T any]() *Result[T] {
	return &Result[T]{Items: make([]T, 0)}
}

func (result Result[T]) Map() map[string]any {
	return map[string]any{
		"total": result.Total, "items": result.Items,
	}
}

// vector
type Vector[T any] []T // T should marshalable

func (items *Vector[T]) Scan(value any) (err error) {
	bts, ok := value.([]byte)
	if !ok {
		return fmt.Errorf("failed to unmarshal JSONB value: %v", value)
	}
	bts = bytes.TrimSpace(bts)

	result := make(Vector[T], 0, 5)
	if len(bts) > 0 { // empty []uint8 cause error
		err = json.Unmarshal(bts, &result)
	}

	*items = result
	return err
}

func (vec Vector[T]) Value() (driver.Value, error) {
	return json.Marshal(vec)
}

// map
type Map[T any] map[string]T

func (item *Map[T]) Scan(value any) (err error) {
	bts, ok := value.([]byte)
	if !ok {
		return fmt.Errorf("failed to unmarshal JSONB value: %v", value)
	}
	bts = bytes.TrimSpace(bts)

	result := make(Map[T], 5)
	if len(bts) > 0 { // empty []uint8 causes error
		err = json.Unmarshal(bts, &result)
	}

	*item = result
	return err
}

func (item Map[T]) Value() (driver.Value, error) {
	return json.Marshal(item)
}

// to avoid use this func, you can't bind field to an pointer rather than a structure
func JSONIsNull(tx *gorm.DB, field string, isNull bool) *gorm.DB {
	where := fmt.Sprintf("JSON_LENGTH(JSON_SEARCH(%s, 'all', '%%'))", field)

	if isNull {
		return tx.Where(where + " IS NULL")
	} else {
		return tx.Where("NOT " + where + " IS NULL")
	}
}

func JSONContains(tx *gorm.DB, field string, val any, contains bool) *gorm.DB {
	where := fmt.Sprintf("JSON_CONTAINS(%s, ?, '$')", field)

	switch val.(type) {
	case string:
		val = fmt.Sprintf(`"%s"`, val)
	default: // number, boolean, TODO: ??
	}

	if contains {
		return tx.Where(where, val)
	} else {
		return tx.Where("NOT "+where, val)
	}
}

func CloseDB(db *gorm.DB) (err error) {
	var sqlDB *sql.DB

	if sqlDB, err = db.DB(); err != nil {
		return err
	}
	return sqlDB.Close()
}
