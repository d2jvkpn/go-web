package kafka

import (
	"context"
	"fmt"

	"github.com/Shopify/sarama"
	"github.com/spf13/viper"
)

type Config struct {
	Addrs   []string `mapstructure:"addrs"`
	Version string   `mapstructure:"version"`  // 3.4.0
	GroupId string   `mapstructure:"group_id"` // default
	Topic   string   `mapstructure:"topic"`
}

func HandlerFromConfig(vp *viper.Viper, field string, ctx context.Context) (
	handler *Handler, err error) {
	var (
		config Config
		cfg    *sarama.Config
		group  sarama.ConsumerGroup
	)

	if err = vp.UnmarshalKey(field, &config); err != nil {
		return nil, err
	}

	if len(config.Addrs) == 0 || config.Version == "" {
		return nil, fmt.Errorf("invlaid addrs or version")
	}

	if config.GroupId == "" || config.Topic == "" {
		return nil, fmt.Errorf("invlaid group_id or topic")
	}

	cfg = sarama.NewConfig()
	if cfg.Version, err = sarama.ParseKafkaVersion(config.Version); err != nil {
		return nil, err
	}

	if group, err = sarama.NewConsumerGroup(config.Addrs, config.GroupId, cfg); err != nil {
		return nil, err
	}

	handler = NewHandler(ctx, group, []string{config.Topic})

	// alter handler.Logger later
	return handler, nil
}
