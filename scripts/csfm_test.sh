#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})


##
make build-csfm

## aliyun oss
rm target/hello.txt || true

target/csfm oss-upload --source pkg/aliyun/examples/hello.txt --target test/hello_2022.txt

target/csfm oss-copy --source test/hello_2022.txt --target test/hello_2022-08.txt

target/csfm oss-delete --source test/hello_2022.txt

target/csfm oss-get --source test/hello_2022-08.txt --target target/hello.txt
cat target/hello.txt

## aws s3
rm target/hello.txt || true

target/csfm s3-upload --source pkg/aws/examples/hello.txt --target test/hello_2022.txt

target/csfm s3-copy --source test/hello_2022.txt --target test/hello_2022-08.txt

target/csfm s3-delete --source test/hello_2022.txt

target/csfm s3-get --source test/hello_2022-08.txt --target target/hello.txt
cat target/hello.txt

target/csfm s3-upload --source pkg/aws/examples/html --target test/hello

target/csfm s3-upload --source pkg/aws/examples/index.html --target test/index.html
