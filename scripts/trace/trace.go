package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/d2jvkpn/go-web/pkg/cloud_native"
	"github.com/d2jvkpn/go-web/pkg/misc"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const ServiceName = "callHello"

func init() {
	misc.RegisterLogPrinter()
}

func main() {
	var (
		otelAddr    string
		httpAddr    string
		err         error
		closeTracer func()
		tracer      trace.Tracer
	)

	flag.StringVar(&otelAddr, "otelAddr", "127.0.0.1:4317", "opentelemetry tracer address")
	flag.StringVar(&httpAddr, "httpAddr", "http://127.0.0.1:3011", "http server address")
	flag.Parse()

	defer func() {
		if err != nil {
			log.Println(err)
			// fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
	}()

	closeTracer, err = cloud_native.LoadTracer(otelAddr, ServiceName, 3*time.Second, false)
	if err != nil {
		return
	}
	defer closeTracer()

	tracer = otel.Tracer(ServiceName)

	for i := 1; i <= 5; i++ {
		ctx, span := tracer.Start(context.Background(), "ExecuteHelloRequest")
		err = callHello(ctx, fmt.Sprintf("%s/api/open/hello?id=%d", httpAddr, i))
		if err != nil {
			break
		}

		opts := []trace.EventOption{
			trace.WithAttributes(attribute.Int("hello", i)),
		}

		fmt.Printf(
			"~~~ callHello: traceId=%s, spanId=%s\n",
			span.SpanContext().TraceID().String(),
			span.SpanContext().SpanID().String(),
		)

		span.AddEvent("successfully finished callHello", opts...)
		span.End()
		time.Sleep(time.Second)
	}
}

func callHello(ctx context.Context, addr string) (err error) {
	var (
		client *http.Client
		req    *http.Request
		res    *http.Response
	)

	client = &http.Client{
		Transport: otelhttp.NewTransport(http.DefaultTransport),
	}

	if req, err = http.NewRequestWithContext(ctx, "GET", addr, nil); err != nil {
		return err
	}

	log.Println("~~~ callHello")
	if res, err = client.Do(req); err != nil {
		return err
	}
	res.Body.Close()

	return nil
}
