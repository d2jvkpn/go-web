package main

//go:generate bash scripts/go_build.sh

import (
	_ "embed"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"runtime/debug"
	"syscall"

	"github.com/d2jvkpn/go-web/internal"
	"github.com/d2jvkpn/go-web/pkg/misc"
	"github.com/d2jvkpn/go-web/pkg/wrap"

	"github.com/spf13/viper"
)

var (
	//go:embed project.yaml
	_Project []byte
)

func init() {
	misc.RegisterLogPrinter()
}

func main() {
	var (
		config, addr string
		version      string
		memLimitMB   int64
		release      bool
		err          error
		errch        chan error
		quit         chan os.Signal
		project      *viper.Viper
	)

	if project, err = wrap.ConfigFromBytes(_Project, "yaml"); err != nil {
		log.Fatalln(err)
	}
	version = project.GetString("version")

	meta := misc.BuildInfo()
	meta["project"] = project.GetString("project")
	meta["version"] = version

	flag.StringVar(&config, "config", "configs/local.yaml", "config file path")
	flag.StringVar(&addr, "addr", ":3011", "http serve address")
	flag.BoolVar(&release, "release", false, "run in release mode")
	flag.Int64Var(&memLimitMB, "memLimitMB", 0, "set memory limit MB")

	flag.Usage = func() {
		output := flag.CommandLine.Output()

		fmt.Fprintf(
			output,
			"%s\n\nUsage of %s:\n",
			misc.BuildInfoText(meta),
			filepath.Base(os.Args[0]),
		)
		flag.PrintDefaults()
		fmt.Fprintf(output, "\nConfig template:\n```yaml\n%s```\n", project.GetString("config"))
	}
	flag.Parse()

	// runtime parameters
	meta["pid"] = os.Getpid()
	meta["-config"] = config
	meta["-addr"] = addr
	meta["-release"] = release
	meta["-memLimitMB"] = memLimitMB

	if memLimitMB > 0 {
		debug.SetMemoryLimit(memLimitMB << 20)
	}

	internal.AppendStaticDir(wrap.ServeStatic("/uploads", "./data/uploads", false))

	if err = internal.Load(config, release); err != nil {
		log.Fatalln(err)
	}

	if errch, err = internal.Serve(addr, meta); err != nil {
		log.Fatalln(err)
	}
	log.Printf(
		">>> HTTP server is listening on %s, pid: %d, version: %s\n",
		addr, os.Getpid(), version,
	)

	quit = make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM, syscall.SIGUSR2)

SELECT:
	select {
	case err = <-errch:
		// ignore err
	case sig := <-quit:
		if sig == syscall.SIGUSR2 {
			p, e := misc.PprofCollect("data/pprof", 10, 100)
			log.Printf("misc.PprofCollect: %s, %v\n", p, e)
			goto SELECT
		}

		fmt.Println("")
		log.Println("<<< Exit")
		errch <- errors.New("SHUTDOWN")
		<-errch
	}

	if err != nil {
		log.Fatalln(err)
	}
}
