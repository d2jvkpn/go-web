package internal

import (
	"github.com/d2jvkpn/go-web/internal/settings"
)

type Cron struct {
	At   string `mapstructure:"at" json:"at,omitemoty"`
	Name string `mapstructure:"name" json:"name,omitemoty"`
}

func _SetupCrons() (err error) {
	// second, minute, hour, day(month), month, day(week)
	// at, jobname := "0 */1 * * * *", "JobName" // every minute, for testing only
	var crons []Cron

	if err = settings.Config.UnmarshalKey("crons", &crons); err != nil {
		return err
	}

	for _, v := range crons {
		_, err = _Cron.AddFunc(v.At, func() {
			// TODO: impls
		})

		if err != nil {
			return err
		}
	}

	//... more cron jobs
	return nil
}
