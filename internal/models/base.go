package models

import (
	"fmt"
	"sync"

	"gorm.io/gorm"
)

var (
	_DB   *gorm.DB   = nil
	_Once *sync.Once = new(sync.Once)
)

func InitDB(db *gorm.DB) (err error) {
	if db == nil {
		return fmt.Errorf("db is nil")
	}

	_Once.Do(func() { _DB = db })
	return nil
}
