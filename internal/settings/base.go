package settings

import (
	"math/rand"
	"time"

	"github.com/d2jvkpn/go-web/pkg/wrap"

	"github.com/allegro/bigcache/v3"
	"github.com/spf13/viper"
)

const (
	App = "go-web"
)

var (
	Rng      *rand.Rand
	Config   *viper.Viper
	Logger   *wrap.Logger
	Cache    *bigcache.BigCache
	Uploader CloudUploader
)

func init() {
	Rng = rand.New(rand.NewSource(time.Now().UnixNano()))

	config := bigcache.DefaultConfig(10 * time.Minute)
	Cache, _ = bigcache.NewBigCache(config)
}
