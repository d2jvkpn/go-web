package settings

import (
	"context"
	"fmt"

	"github.com/d2jvkpn/go-web/pkg/aliyun"
	"github.com/d2jvkpn/go-web/pkg/aws"
)

type CloudUploader interface {
	Name() string
	BucketUrl() string

	Upload(context.Context, string, string) (string, error)
	GetSts(context.Context, string) (*StsResult, error)
	Copy(context.Context, string, string) (string, error)
	Delete(context.Context, string) error
}

type StsResult aws.StsResult

func stsOss2S3(res *aliyun.StsResult) (result *StsResult) {
	return &StsResult{
		AccessKeyId:     res.AccessKeyId,
		SecretAccessKey: res.AccessKeySecret,
		SessionToken:    res.SecurityToken,
		Expiration:      res.Expiration,
		Region:          res.Region,
		Bucket:          res.Bucket,
	}
}

// aliyun.Oss impl
type Oss struct {
	*aliyun.OssClient
}

func (oss Oss) Name() string {
	return "aliyun_oss"
}

func (oss Oss) Upload(ctx context.Context, source, target string) (link string, err error) {
	if link, err = oss.OssClient.Upload(source, target); err != nil {
		return "", err
	}

	return link, nil
}

func (oss Oss) GetSts(ctx context.Context, sessionName string) (result *StsResult, err error) {
	var res *aliyun.StsResult
	if res, err = oss.OssClient.GetSts(sessionName); err != nil {
		return nil, err
	}
	return stsOss2S3(res), nil
}

func (oss Oss) Copy(ctx context.Context, source, target string) (code string, err error) {
	return oss.OssClient.Copy(source, target)
}

func (oss Oss) Delete(ctx context.Context, source string) error {
	return oss.OssClient.Delete(source)
}

func NewAliyunOss(fp, field string) (uploader CloudUploader, err error) {
	var cli *aliyun.OssClient
	if cli, err = aliyun.NewOssClient(fp, field); err != nil {
		return nil, fmt.Errorf("NewAliyunOss: %w", err)
	}
	return Oss{cli}, nil
}

// aws.S3 impl
type S3 struct {
	*aws.S3Client
}

func (s3 S3) Name() string {
	return "aws_s3"
}

func (s3 S3) Upload(ctx context.Context, source, target string) (link string, err error) {
	if link, err = s3.S3Client.Upload(ctx, source, target); err != nil {
		return "", err
	}
	return link, nil
}

func (s3 S3) GetSts(ctx context.Context, sessionName string) (result *StsResult, err error) {
	var res *aws.StsResult
	if res, err = s3.S3Client.GetSts(ctx, sessionName); err != nil {
		return nil, err
	}
	*result = StsResult(*res)
	return result, nil
}

func (s3 S3) Copy(ctx context.Context, source, target string) (code string, err error) {
	return s3.S3Client.Copy(ctx, source, target)
}

func (s3 S3) Delete(ctx context.Context, source string) error {
	return s3.S3Client.Delete(ctx, source)
}

func NewAwsS3(fp, field string) (uploader CloudUploader, err error) {
	var cli *aws.S3Client
	if cli, err = aws.NewS3Client(fp, field); err != nil {
		return nil, fmt.Errorf("NewAwsS3: %w", err)
	}
	return S3{cli}, nil
}
