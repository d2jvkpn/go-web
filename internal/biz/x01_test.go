package biz

import (
	// "fmt"
	"testing"

	. "github.com/stretchr/testify/require"
)

func TestSliceV1(t *testing.T) {
	input := []int{1, 2, 3}

	output := SliceV1(input...)
	Equal(t, input, output)
}

func BenchmarkSliceV1(b *testing.B) {
	input := []int{}

	for i := 0; i < 1000; i++ {
		input = append(input, i)
	}

	for i := 0; i < b.N; i++ {
		_ = SliceV1(input...)
	}
}

func BenchmarkSliceV2(b *testing.B) {
	input := []int{}

	for i := 0; i < 1000; i++ {
		input = append(input, i)
	}

	for i := 0; i < b.N; i++ {
		_ = SliceV2(input...)
	}
}
