package biz

func SliceV1(items ...int) []int {
	ans := make([]int, 0)

	for _, v := range items {
		ans = append(ans, v)
	}

	return ans
}

func SliceV2(items ...int) []int {
	ans := make([]int, 0, len(items))

	for _, v := range items {
		ans = append(ans, v)
	}

	return ans
}
