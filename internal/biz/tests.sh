#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})

cd ${_path}

go test -run ^TestSliceV1$

go test -run none --bench ^BenchmarkSliceV1$ -count 5 -cpu 1,2 -benchmem
#goos: linux
#goarch: amd64
#pkg: github.com/d2jvkpn/go-web/internal/biz
#cpu: AMD Ryzen 7 4800H with Radeon Graphics         
#BenchmarkSliceV1     	  231312	      5164 ns/op	   25208 B/op	      12 allocs/op
#BenchmarkSliceV1     	  242588	      5211 ns/op	   25208 B/op	      12 allocs/op
#BenchmarkSliceV1     	  245257	      5599 ns/op	   25208 B/op	      12 allocs/op
#BenchmarkSliceV1     	  238741	      5189 ns/op	   25208 B/op	      12 allocs/op
#BenchmarkSliceV1     	  242766	      5241 ns/op	   25208 B/op	      12 allocs/op
#BenchmarkSliceV1-2   	  237822	      5022 ns/op	   25208 B/op	      12 allocs/op
#BenchmarkSliceV1-2   	  246128	      5061 ns/op	   25208 B/op	      12 allocs/op
#BenchmarkSliceV1-2   	  242746	      5102 ns/op	   25208 B/op	      12 allocs/op
#BenchmarkSliceV1-2   	  242227	      5134 ns/op	   25208 B/op	      12 allocs/op
#BenchmarkSliceV1-2   	  242590	      5075 ns/op	   25208 B/op	      12 allocs/op
#PASS
#ok  	github.com/d2jvkpn/go-web/internal/biz	14.440s


go test -run none --bench ^BenchmarkSliceV2$ -count 5 -cpu 1,2 -benchmem
#goos: linux
#goarch: amd64
#pkg: github.com/d2jvkpn/go-web/internal/biz
#cpu: AMD Ryzen 7 4800H with Radeon Graphics         
#BenchmarkSliceV2     	  683479	      1708 ns/op	    8192 B/op	       1 allocs/op
#BenchmarkSliceV2     	  699482	      1800 ns/op	    8192 B/op	       1 allocs/op
#BenchmarkSliceV2     	  694346	      1803 ns/op	    8192 B/op	       1 allocs/op
#BenchmarkSliceV2     	  741900	      1784 ns/op	    8192 B/op	       1 allocs/op
#BenchmarkSliceV2     	  694831	      1800 ns/op	    8192 B/op	       1 allocs/op
#BenchmarkSliceV2-2   	  726766	      1820 ns/op	    8192 B/op	       1 allocs/op
#BenchmarkSliceV2-2   	  636372	      1874 ns/op	    8192 B/op	       1 allocs/op
#BenchmarkSliceV2-2   	  729762	      1905 ns/op	    8192 B/op	       1 allocs/op
#BenchmarkSliceV2-2   	  726662	      1856 ns/op	    8192 B/op	       1 allocs/op
#BenchmarkSliceV2-2   	  728437	      1821 ns/op	    8192 B/op	       1 allocs/op
#PASS
#ok  	github.com/d2jvkpn/go-web/internal/biz	14.761s
