package biz

import (
	"context"
	// "fmt"

	. "github.com/d2jvkpn/go-web/pkg/resp"
)

type Query struct {
	PageNo   uint16 `json:"pageNo,omitempty" form:"pageNo"`
	PageSize uint16 `json:"pageSize,omitempty" form:"pageSize"`
	Status   string `json:"status,omitempty" form:"status"`
}

type Result struct{}

func (query *Query) Valid() error {
	return nil
}

func (query *Query) Do(ctx context.Context) (*Result, *Error) {
	return &Result{}, nil
}
