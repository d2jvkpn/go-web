package biz

import (
	"context"
	// "fmt"
	"strings"
	"time"

	"github.com/d2jvkpn/go-web/internal/settings"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

func Hello(ctx context.Context, idx uint64) (name string) {
	name = _Names[int(idx)%len(_Names)]

	ServiceB(ctx, 42)

	commonLabels := []attribute.KeyValue{
		attribute.String("biz.Hello:", strings.ToUpper(name)),
	}

	span := trace.SpanFromContext(ctx)
	span.SetAttributes(commonLabels...)

	// traceId := span.SpanContext().TraceID().String()
	// spanId := span.SpanContext().SpanID().String()
	// fmt.Println("~~~ biz.Hello:", traceId, spanId)

	if settings.Config.GetBool("opentelemetry.enable") {
		settings.Logger.Info(
			"biz.Hello",
			zap.String("traceId", span.SpanContext().TraceID().String()),
			zap.String("spanId", span.SpanContext().SpanID().String()),
		)
	}

	return
}

func ServiceB(ctx context.Context, val int64) {
	tracer := otel.Tracer("ServiceB")

	_, span := tracer.Start(ctx, "serviceB.xxxx")
	defer span.End()
	time.Sleep(42 * time.Millisecond)

	//	traceId := span.SpanContext().TraceID().String()
	//	spanId := span.SpanContext().SpanID().String()
	//	fmt.Println("~~~ biz.ServiceB:", traceId, spanId)

	if !settings.Config.GetBool("opentelemetry.enable") {
		return
	}

	settings.Logger.Info(
		"biz.ServiceB",
		zap.String("traceId", span.SpanContext().TraceID().String()),
		zap.String("spanId", span.SpanContext().SpanID().String()),
	)

	// write to log
	opts := []trace.EventOption{
		trace.WithAttributes(attribute.Int64("serviceB.val", val)),
	}

	span.AddEvent("successfully finished call ServiceB", opts...)
}
