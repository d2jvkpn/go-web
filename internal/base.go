package internal

import (
	// "fmt"
	"embed"
	"expvar"
	"net/http"
	"runtime"
	"time"

	"github.com/d2jvkpn/go-web/pkg/wrap"

	"github.com/robfig/cron/v3"
	"go.uber.org/zap"
)

const (
	HTTP_MaxHeaderBytes     = 2 << 11 // 4K
	HTTP_ReadHeaderTimeout  = 2 * time.Second
	HTTP_ReadTimeout        = 10 * time.Second
	HTTP_WriteTimeout       = 10 * time.Second
	HTTP_IdleTimeout        = 60
	HTTP_MaxMultipartMemory = 8 << 20 // 8M

	LOG_SizeMB = 512
)

var (
	//go:embed static
	_Static embed.FS
	//go:embed templates
	_Templates  embed.FS
	_StaticDirs []wrap.StaticDir

	_Release bool

	_Cron   *cron.Cron
	_Server *http.Server
	_Logger *zap.Logger

	_CloseTracer func()
)

func init() {
	// _InstanceId = wrap.RandString(16)
	_Cron = cron.New(cron.WithSeconds())
	_StaticDirs = make([]wrap.StaticDir, 0)
}

func setExpvars() {
	expvar.Publish("goroutines", expvar.Func(func() any {
		return runtime.NumGoroutine()
	}))

	expvar.Publish("timestamp", expvar.Func(func() any {
		return time.Now().Format(time.RFC3339)
	}))

	// export memstats and cmdline by default
	//	expvar.Publish("memStats", expvar.Func(func() any {
	//		memStats := new(runtime.MemStats)
	//		runtime.ReadMemStats(memStats)
	//		return memStats
	//	}))
}
