package internal

import (
	"fmt"
	"net/http"
	// "os"

	"github.com/d2jvkpn/go-web/internal/settings"
	"github.com/d2jvkpn/go-web/pkg/wrap"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func Load(fp string, release bool) (err error) {
	///
	var engi *gin.Engine

	if !release {
		// _ = os.Setenv("APP_DebugMode", "true")
		settings.Logger = wrap.NewLogger("logs/go-web.log", zap.DebugLevel, LOG_SizeMB, nil)
	} else {
		// _ = os.Setenv("APP_DebugMode", "false")
		settings.Logger = wrap.NewLogger("logs/go-web.log", zap.InfoLevel, LOG_SizeMB, nil)
	}
	_Release = release

	///
	if settings.Config, err = wrap.OpenConfig(fp); err != nil {
		return
	}

	switch settings.Config.GetString("cloud_provider") {
	case "aws":
		settings.Uploader, err = settings.NewAwsS3(fp, "aws_s3")
	case "aliyun":
		settings.Uploader, err = settings.NewAliyunOss(fp, "aliyun_oss")
	default:
		return fmt.Errorf("unkonwn cloud_provider in config")
	}
	if err != nil {
		return err
	}

	if err = _SetupCrons(); err != nil {
		return err
	}

	setExpvars()

	if engi, err = NewEngine(_Release); err != nil {
		return err
	}

	///
	_Server = &http.Server{ // TODO: set consts in base.go
		ReadTimeout:       HTTP_ReadTimeout,
		WriteTimeout:      HTTP_WriteTimeout,
		ReadHeaderTimeout: HTTP_ReadHeaderTimeout,
		MaxHeaderBytes:    HTTP_MaxHeaderBytes,
		// Addr:              addr,
		Handler: engi,
	}

	return
}
