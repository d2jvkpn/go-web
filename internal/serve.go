package internal

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"sync"
	"time"

	"github.com/d2jvkpn/go-web/internal/settings"
	"github.com/d2jvkpn/go-web/pkg/cloud_native"
	"github.com/d2jvkpn/go-web/pkg/wrap"

	"go.uber.org/zap"
)

func AppendStaticDir(dirs ...wrap.StaticDir) {
	if len(dirs) == 0 {
		return
	}

	_StaticDirs = append(_StaticDirs, dirs...)
}

func Serve(addr string, meta map[string]any) (errch chan error, err error) {
	var listener net.Listener

	_Cron.Start()

	_Logger = settings.Logger.Logger.Named("serve")
	_Logger.Info("Server is starting", zap.Any("meta", meta))

	if settings.Config.GetBool("opentelemetry.enable") {
		str := settings.Config.GetString("opentelemetry.address")
		secure := settings.Config.GetBool("opentelemetry.secure")
		_CloseTracer, err = cloud_native.LoadTracer(str, settings.App, 3*time.Second, secure)
		if err != nil {
			return nil, fmt.Errorf("cloud_native.LoadTracer: %s, %w", str, err)
		}
	}

	msg := fmt.Sprintf("HTTP server is listening on %s", addr)
	_Logger.Info(msg)
	// _Server.Addr = addr

	if listener, err = net.Listen("tcp", addr); err != nil {
		return nil, err
	}

	once := new(sync.Once)

	shutdown := func() {
		if _Server != nil {
			ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
			if err := _Server.Shutdown(ctx); err != nil {
				_Logger.Error(fmt.Sprintf("server shutdown: %v", err))
			}
			cancel()
		}
	}

	errch = make(chan error, 2)

	go func() {
		if err := _Server.Serve(listener); err != http.ErrServerClosed {
			once.Do(onExit)
			errch <- err
		}
	}()

	go func() {
		if err := <-errch; err.Error() == "SHUTDOWN" {
			shutdown()
			once.Do(onExit)
			errch <- err
		}
	}()

	return errch, nil
}

func onExit() {
	_Cron.Stop()

	if _CloseTracer != nil {
		_CloseTracer()
	}

	// close other goroutines or services
	settings.Logger.Down()
}
