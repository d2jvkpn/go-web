package api

import (
	"context"
	// "log"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/d2jvkpn/go-web/internal/biz"
	"github.com/d2jvkpn/go-web/internal/settings"
	. "github.com/d2jvkpn/go-web/pkg/resp"

	"github.com/gin-gonic/gin"
	"github.com/tidwall/gjson"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

func hello(ctx *gin.Context) {
	var (
		cached    bool
		id        uint64
		httpCode  int
		bts       []byte
		key, name string
		err       error
		reqCtx    context.Context
	)

	reqCtx = ctx.Request.Context()
	id, _ = strconv.ParseUint(ctx.DefaultQuery("id", "0"), 10, 64)
	key = fmt.Sprintf("api.hello:%d", id)

	// key := "Authorization"
	// log.Printf("~~~ Header %s: %s\n", key, ctx.GetHeader(key))
	// ctx.JSON(http.StatusOK, gin.H{"code": 0, "msg": "ok", "data": gin.H{}})
	// Ok(ctx)

	// open.GET("/hello/:name", hello)
	//	name := ctx.Param("name")
	//	if name == "" {
	//		name = "Joe Doe"
	//	}

	time.Sleep(24 * time.Millisecond)

	if bts, err = settings.Cache.Get(key); err == nil {
		cached, httpCode = true, int(bts[0])
		bts = bts[1:]
		name = gjson.GetBytes(bts, "name").String()
	} else {
		settings.Logger.Error(
			"api.hello-settings.Cache.Get",
			zap.Uint64("id", id), zap.Any("error", err),
		)

		httpCode = 200
		name = biz.Hello(reqCtx, id)
		bts, _ = json.Marshal(gin.H{"name": name})
		_ = settings.Cache.Set(key, append([]byte{byte(httpCode)}, bts...))
	}

	commonLabels := []attribute.KeyValue{
		attribute.String("api.hello", name),
	}

	span := trace.SpanFromContext(reqCtx)
	span.SetAttributes(commonLabels...)

	traceId := span.SpanContext().TraceID().String()
	spanId := span.SpanContext().SpanID().String()
	// fmt.Println("~~~ api.hello:", traceId, spanId)

	fields := make([]zap.Field, 0, 5)
	fields = append(
		fields, zap.Uint64("id", id), zap.String("name", name), zap.Bool("cached", cached),
	)
	if settings.Config.GetBool("opentelemetry.enable") {
		fields = append(fields, zap.String("traceId", traceId), zap.String("spanId", spanId))
	}
	settings.Logger.Info("api.hello", fields...)

	//	ctx.Set(KEY_Event, map[string]any{
	//		"traceId": traceId, "spanId": spanId,
	//		"id": id, "name": name, "cached": cached,
	//	})

	time.Sleep(24 * time.Millisecond)

	ctx.Header("StatusCode", strconv.Itoa(httpCode))
	// ctx.Header("Status", http.StatusText(http.StatusOK))
	ctx.Header("Content-Type", "application/json; charset=utf-8")
	ctx.Writer.Write(bts)
	// JSON(ctx, gin.H{"name": name}, nil)
	// span.End() // don't do this as it will be called by the middleware
}

/*
	tracer := otel.Tracer("go-web")
	ctx, span := tracer.Start(context.Background(), "ExecuteRequest")
	hello(ctx)
	opts := []{trace.WithAttributes(attribute.String("someKey", "someValue")))}
	span.AddEvent("successfully finished request hello", opts...)
	span.End()
*/

func login(ctx *gin.Context) {
	//	ctx.JSON(http.StatusOK, gin.H{
	//		"code": 0, "msg": "ok", "data": gin.H{"token": "xxxxxxxx"},
	//	})

	key := "X-Token" // "Authorization"
	val := ctx.GetHeader(key)
	// log.Printf("~~~ Header %s: %s\n", key, ctx.GetHeader(key))
	ctx.Set(KEY_UserId, "yyyyyyyy")

	switch {
	case val == "":
		BadRequest(ctx, fmt.Errorf("missing header: %s", key))
	case len(val) != 8:
		HandleError(ctx, ErrBadRequest(fmt.Errorf("invalid X-Token"), ""))
	case val == "xxxxxxxx":
		ctx.Set(KEY_Event, "user logined success")
		JSON(ctx, gin.H{key: val}, nil)
	default:
		err := NewError( // biz error
			fmt.Errorf("failed to parse token"),
			http.StatusInternalServerError,
			1,
			Msg("sorry"),
		)

		HandleError(ctx, err)
	}
}
