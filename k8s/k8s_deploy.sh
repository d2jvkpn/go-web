#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})

# https://github.com/d2jvkpn/exercises/tree/master/2022_k8s_on_kvm

kubectl create ns dev
kubectl create ns test

#### 1. k8s configmap
kubectl config set-context --current --namespace=dev

kubectl create configmap go-web --from-file=dev.yaml
kubectl get configmap go-web
kubectl get cm go-web -o yaml

#### 2. k8s deployment
kubectl apply -f deploy-dev.yaml
kubectl get deploy/go-web

kubectl apply -f cluster-ip.yaml
kubectl get svc/go-web
kubectl -n test get svc/go-web

kubectl apply -f ingress.yaml
kubectl get ingress
kubectl -n dev get ingress/go-web -o yaml

# curl -H "Host: dev.go-web.local" http://localhost/api/open/hello

#### 3. k8s node-port
kubectl apply -f node-port.yaml
#### vm3 host: 192.168.122.146
# curl -i 192.168.122.146:30012/api/open/hello

#### 4. misc
kubectl scale deploy/go-web --replicas=1

kubectl get deploy/go-web -o yaml
kubectl get pods
# kubectl -n dev exec -it pod/go-web-7dcd4f7897-2jjqd -- sh
# kubectl -n dev exec pod/go-web-7dcd4f7897-2jjqd -- sh -c 'echo "hello, world!" > data/hello.txt'

kubectl get no -o wide --show-labels

kubectl -n dev get pod -o wide --show-labels

#### 5. ssl
kubectl create secret tls go-web --key go-web.local.key --cert go-web.local.cer
# $ kubectl -n dev delete secret go-web --ignore-not-found
# update a secret
# $ kubectl -n dev create secret tls go-web --dry-run=client   \
#   --key 'go-web.local.key' --cert 'go-web.local.cer' -o yaml |
#   kubectl apply -f -

_=`
apiVersion: v1
kind: Secret
metadata:
  name: hello-app-tls
  namespace: dev
type: kubernetes.io/tls
data:
  server.crt: |
    <crt contents here>
  server.key: |
    <private key contents here>
`

kubectl -n dev get secret/go-web

# default ssl cert
ns=ingress-nginx
pod=$(kubectl -n $ns get pods | grep "ingress.*Running" | head -1 | cut -f 1 -d " ")
kubectl -n $ns exec -it $pod -- cat /etc/nginx/nginx.conf | grep ssl_cert

curl -kv https://dev.go-web.local

kubect delete -f ingress.yaml
kubect apply -f ingress-tls.yaml


#### 6. using a private registry
kubectl create secret docker-registry my-registry \
  --docker-server="REGISTRY.SITE.COM" --docker-email="EMAIL" \
  --docker-username="USERNAME" --docker-password="PASSWORD"

# spec.template.spec
_=`    imagePullSecrets:
      - name: my-registry
`
