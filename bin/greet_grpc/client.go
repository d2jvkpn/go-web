package main

import (
	"context"
	"fmt"
	"log"

	. "github.com/d2jvkpn/go-web/pkg/greet"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func runClient(ctx context.Context, addr string, funcs []string) {
	var (
		err    error
		conn   *grpc.ClientConn
		client *GreetClient
	)

	inte := ClientInterceptor{
		Methods: map[string]bool{
			"/greet.GreetService/Greet": true,
		},
		Token: "hello",
	}

	conn, err = grpc.Dial(addr,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(inte.Unary()),
		grpc.WithStreamInterceptor(inte.Stream()),
	)

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(">> Dial:", addr)

	defer func() {
		conn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	client = NewGreetClient(conn)

	if len(funcs) == 0 {
		if err = client.Greet(ctx); err != nil {
			return
		}
		if err = client.GreetStream(ctx); err != nil {
			return
		}
		if err = client.Multiply(ctx); err != nil {
			return
		}
		if err = client.Hello(ctx); err != nil {
			return
		}
		if err = client.Upload(ctx); err != nil {
			return
		}
		return
	}

	switch funcs[0] {
	case "greet":
		if err = client.Greet(ctx); err != nil {
			return
		}
	case "greetStream":
		if err = client.GreetStream(ctx); err != nil {
			return
		}
	case "multiply":
		if err = client.Multiply(ctx); err != nil {
			return
		}
	case "hello":
		if err = client.Hello(ctx); err != nil {
			return
		}
	case "upload":
		if err = client.Upload(ctx); err != nil {
			return
		}
	default:
		err = fmt.Errorf("allowed functionss: greet, greetStream, multiply, hello, upload")
	}
}

type ClientInterceptor struct {
	Methods map[string]bool
	Token   string
}

func (inte *ClientInterceptor) attachToken(ctx context.Context) context.Context {
	return metadata.AppendToOutgoingContext(ctx, "authorization", inte.Token)
}

func (inte *ClientInterceptor) Unary() grpc.UnaryClientInterceptor {
	return func(
		ctx context.Context, method string, req, reply any,
		cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption,
	) (err error) {

		fmt.Printf("=== ClientInterceptor.Unary: %+v\n", method)

		if inte.Methods[method] {
			return invoker(inte.attachToken(ctx), method, req, reply, cc, opts...)
		}

		return invoker(ctx, method, req, reply, cc, opts...)
	}
}

func (inte *ClientInterceptor) Stream() grpc.StreamClientInterceptor {
	return func(
		ctx context.Context, desc *grpc.StreamDesc, cc *grpc.ClientConn,
		method string, streamer grpc.Streamer, opts ...grpc.CallOption,
	) (client grpc.ClientStream, err error) {

		fmt.Printf("=== ClientInterceptor.Stream: %+v\n", method)

		if inte.Methods[method] {
			return streamer(inte.attachToken(ctx), desc, cc, method, opts...)
		}

		return streamer(ctx, desc, cc, method, opts...)
	}
}
