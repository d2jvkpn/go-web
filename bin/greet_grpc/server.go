package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	. "github.com/d2jvkpn/go-web/pkg/greet"
	"github.com/d2jvkpn/go-web/pkg/misc"
	"github.com/d2jvkpn/go-web/pkg/wrap"

	"go.uber.org/multierr"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func runServer(addr, consul string) {
	var (
		port int
		err  error

		listener net.Listener
		srv      *grpc.Server
		cc       *wrap.ConsulClient
	)

	if consul != "" {
		if port, err = misc.PortFromAddr(addr); err != nil {
			log.Fatalln(err)
		}
		if cc, err = wrap.NewConsulClient(consul, "consul"); err != nil {
			log.Fatalln(err)
		}
	}

	///
	if listener, err = net.Listen("tcp", addr); err != nil {
		log.Fatal(err)
	}

	inte := ServerInterceptor{
		Methods: map[string]bool{"/greet.GreetService/Greet": true},
		Tokens:  []string{"hello", "world", "2022"},
	}

	srv = grpc.NewServer(
		// grpc.UnaryInterceptor(unaryServerInterceptor),
		// grpc.StreamInterceptor(streamServerInterceptor),
		grpc.UnaryInterceptor(inte.Unary()),
		grpc.StreamInterceptor(inte.Stream()),
	)
	RegisterGreetServiceServer(srv, &Server{}) // RegisterService

	if cc != nil && cc.Registry {
		if err = cc.GRPCRegister(port, false, srv); err != nil {
			log.Fatalln(err)
		}
	}

	errch, quit := make(chan error, 1), make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

	go func() {
		log.Printf(">>> Greet RPC server: %q\n", addr)
		errch <- srv.Serve(listener)
	}()

	select {
	case err = <-errch:
	case <-quit:
		srv.GracefulStop()
		err = <-errch
	}

	if cc != nil && cc.Registry {
		if e := cc.Deregister(); e != nil {
			err = multierr.Append(err, e)
		}
	}

	if err != nil {
		log.Fatalln(err)
	}
}

// server interceptors
func unaryServerInterceptor(
	ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (
	resp any, err error) {

	fmt.Printf("=== unaryServerInterceptor: %+v\n", info)
	return handler(ctx, req)
}

func streamServerInterceptor(
	srv any, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) (
	err error) {

	fmt.Printf("=== streamServerInterceptor: %+v\n", info)
	return handler(srv, ss)
}

// demo server interceptor
type ServerInterceptor struct {
	Methods map[string]bool
	Tokens  []string
}

func (inte *ServerInterceptor) authorize(ctx context.Context, method string) (err error) {
	if !inte.Methods[method] {
		return nil
	}

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return status.Errorf(codes.Unauthenticated, "metadata is not provided")
	}

	values := md["authorization"]
	if len(values) == 0 {
		return status.Errorf(codes.Unauthenticated, "authorization token is not provided")
	}

	for _, v := range inte.Tokens {
		if values[0] == v {
			return nil
		}
	}

	return status.Error(codes.PermissionDenied, "no permissions to access this RPC")
}

func (inte *ServerInterceptor) Unary() grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (
		resp any, err error) {

		fmt.Printf("=== ServerInterceptor.Unary: %+v\n", info)
		if err := inte.authorize(ctx, info.FullMethod); err != nil {
			return nil, err
		}

		return handler(ctx, req)
	}
}

func (inte *ServerInterceptor) Stream() grpc.StreamServerInterceptor {
	return func(
		srv any, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler,
	) (err error) {

		fmt.Printf("=== ServerInterceptor.Stream: %+v\n", info)
		if err := inte.authorize(ss.Context(), info.FullMethod); err != nil {
			return err
		}

		return handler(srv, ss)
	}
}
