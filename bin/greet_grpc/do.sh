#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})


go run ./ server --consul consul.yaml

go run ./ client


exit

policy: greet
```
service "greet" {
	policy = "write"
}
service "greet-sidecar-proxy" {
	policy = "write"
}
service_prefix "" {
	policy = "read"
}
node_prefix "" {
	policy = "read"
}
```
