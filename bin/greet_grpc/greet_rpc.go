package main

import (
	"context"
	"flag"
	"fmt"
	"os"

	"github.com/d2jvkpn/go-web/pkg/misc"
)

func init() {
	misc.RegisterLogPrinter()
}

// $ go run . server --consul http://127.0.0.1:8500
func main() {
	var (
		addr   string
		consul string
	)

	serverCmd := flag.NewFlagSet("server", flag.ExitOnError)
	clientCmd := flag.NewFlagSet("client", flag.ExitOnError)

	if len(os.Args) < 2 {
		fmt.Println("greet_rpc <server|client> [func]")
		os.Exit(1)
	}

	switch os.Args[1] {
	case "server":
		serverCmd.StringVar(&addr, "addr", ":50051", "rpc server address")
		serverCmd.StringVar(&consul, "consul", "", "consul config file")
		serverCmd.Parse(os.Args[2:])
		// server.Parsed() is true
		runServer(addr, consul)
	case "client":
		clientCmd.StringVar(&addr, "addr", "localhost:50051", "rpc server address")
		clientCmd.Parse(os.Args[2:])
		// client.Parsed() is true
		runClient(context.Background(), addr, clientCmd.Args())
	default:
		fmt.Println("subcommand server or client is required")
		flag.PrintDefaults()
		os.Exit(2)
	}
}
