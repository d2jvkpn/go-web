#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})

go run my-service.go -consul consul.yaml -addr :8081
go run my-service.go -consul consul.yaml -addr :8082
go run my-service.go -consul consul.yaml -addr :8083

curl --request GET http://localhost:8500/v1/agent/health/service/name/my-service

exit

Key/Value::Create

Key/Value: my-service/dev.yaml
```
hello: world
```

Tokens::Create -> Create new policy

Policy: my-service
```
key_prefix "my-service/" {
  policy = "read"
}
service "my-service" {
	policy = "write"
}
service "my-service-sidecar-proxy" {
	policy = "write"
}
service_prefix "" {
	policy = "read"
}
node_prefix "" {
	policy = "read"
}
```
