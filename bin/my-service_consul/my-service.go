package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/d2jvkpn/go-web/pkg/misc"
	"github.com/d2jvkpn/go-web/pkg/wrap"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"go.uber.org/multierr"
)

func main() {
	var (
		port   int
		config string
		addr   string
		consul string
		err    error

		cc   *wrap.ConsulClient
		vc   *viper.Viper
		engi *gin.Engine
	)

	flag.StringVar(&addr, "addr", ":8081", "http listening address")
	flag.StringVar(&config, "config", "configs/local.yaml", "config file path")
	flag.StringVar(&consul, "consul", "", "privide a consul config if you use it")
	flag.Parse()

	if consul != "" {
		if port, err = misc.PortFromAddr(addr); err != nil {
			log.Fatalln(err)
		}

		if cc, err = wrap.NewConsulClient(consul, "consul"); err != nil {
			log.Fatalln(err)
		}

		if vc, err = cc.GetKV(cc.Name + "/dev.yaml"); err != nil {
			log.Fatalln(err)
		}
	} else {
		if vc, err = wrap.OpenConfig(config); err != nil {
			log.Fatalln(err)
		}
	}
	fmt.Printf("~~~ config.hello: %v\n", vc.GetString("hello"))

	engi = gin.Default()
	router := &engi.RouterGroup

	router.GET("/health", func(ctx *gin.Context) {
		ctx.AbortWithStatus(http.StatusOK)
	})

	// fmt.Printf("~~~ TLSConfig: %#v\n", svc.ServerTLSConfig())
	// Creating an HTTP server that serves via Connect
	server := &http.Server{
		Addr: addr,
		// TLSConfig: svc.ServerTLSConfig(),
		Handler: engi,
		// ... other standard fields
	}

	if cc != nil && cc.Registry {
		if err = cc.HTTPRegister(port, false, "/health"); err != nil {
			log.Fatalln(err)
		}
	}

	errch, quit := make(chan error, 1), make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

	shutdown := func() {
		ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
		if err := server.Shutdown(ctx); err != nil {
			log.Printf("server shutdown: %v\n", err)
		}
		cancel()
	}

	go func() {
		log.Printf(">>> HTTP server listening on: %s\n", addr)
		if err = server.ListenAndServe(); err != http.ErrServerClosed {
			shutdown()
		} else {
			err = nil
		}
		errch <- err
	}()

	select {
	case err = <-errch:
	case <-quit:
		shutdown()
		err = <-errch
	}

	if cc != nil && cc.Registry {
		if e := cc.Deregister(); e != nil {
			err = multierr.Append(err, e)
		}
	}
	if err != nil {
		log.Fatalln(err)
	}
}
