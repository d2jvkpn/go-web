package main

import (
	"encoding/json"
	"fmt"
	"sort"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

type VersionInfo map[string]any

func (items VersionInfo) String() string {
	slice := make([]string, 0, len(items))

	for k, v := range items {
		slice = append(slice, fmt.Sprintf("%s: %s", k, v))
	}
	sort.Strings(slice)

	return strings.Join(slice, "\n")
}

func (items VersionInfo) JSON() []byte {
	bts, _ := json.Marshal(items)
	return bts
}

func NewVersion(buildInfo map[string]any) (command *cobra.Command) {
	var (
		jsonFmt bool
		fSet    *pflag.FlagSet
	)

	command = &cobra.Command{
		Use:   "version",
		Short: "version inforamtion",
		Long:  `version and build information`,

		Run: func(cmd *cobra.Command, args []string) {
			items := VersionInfo(buildInfo)

			if jsonFmt {
				fmt.Printf("%s\n", items.JSON())
			} else {
				fmt.Printf("%s\n", items)
			}
		},
	}

	fSet = command.Flags()
	fSet.BoolVar(&jsonFmt, "json", false, "output command in json object")

	return command
}
