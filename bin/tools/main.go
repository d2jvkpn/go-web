package main

import (
	_ "embed"

	"github.com/d2jvkpn/go-web/pkg/misc"

	"github.com/spf13/cobra"
)

func init() {
	misc.RegisterLogPrinter()
}

func main() {
	root := &cobra.Command{Use: "tools"}

	root.AddCommand(
		NewApiTest(),
		NewWsTest(),
		NewVersion(misc.BuildInfo()),
		NewPrint("print"),
		NewLog2Tsv(),
	)

	root.Execute()
}
