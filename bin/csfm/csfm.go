package main

import (
	"context"
	_ "embed"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/d2jvkpn/go-web/pkg/aliyun"
	"github.com/d2jvkpn/go-web/pkg/aws"
	"github.com/d2jvkpn/go-web/pkg/wrap"

	"github.com/spf13/viper"
)

var (
	//go:embed project.yaml
	_Project []byte

	_Help string = `Cloud storage file manager: %s
Version: %s
Subcommands format: {Provider}-{Action}
  providers: oss, s3
  actions: upload, get, copy, delete

Configuration(configs/csfm.yaml):
cat <<EOF
%s
%s
EOF

`
)

func main() {
	var (
		config, section  string
		source, target   string
		provider, action string
		found            bool
		res              string
		err              error
		ctx              context.Context
		command          *flag.FlagSet
		scfm             *SCFM
		project          *viper.Viper
	)

	if project, err = wrap.ConfigFromBytes(_Project, "yaml"); err != nil {
		log.Fatalln(err)
	}

	// oss := flag.NewFlagSet("oss", flag.ExitOnError)
	// s3 := flag.NewFlagSet("s3", flag.ExitOnError)
	command = flag.NewFlagSet("csfm", flag.ExitOnError)

	command.StringVar(
		&config, "config", "configs/csfm.yaml",
		"cloud access credentials(aliyun_oss, aws_s3)",
	)

	command.StringVar(&source, "source", "", "source path (required)")
	command.StringVar(&target, "target", "", "target path (required)")
	command.StringVar(&section, "section", "", "default: aliyun_oss or aws_s3")

	if len(os.Args) < 2 {
		fmt.Printf(
			_Help,
			project.GetString("project"), project.GetString("version"),
			aliyun.ConfigDemo(), aws.ConfigDemo(),
		)

		command.PrintDefaults()
		os.Exit(1)
	}

	command.Parse(os.Args[2:])

	if source == "" {
		log.Fatalln("-source can't be empty")
	}

	if provider, action, found = strings.Cut(os.Args[1], "-"); !found {
		log.Fatalln(`invalid command format`)
	}

	if scfm, err = NewSCFM(provider, config, section); err != nil {
		log.Fatalln(err)
	}

	ctx = context.Background()
	defer func() {
		if err != nil {
			log.Fatalln(err)
		}
	}()

	switch action {
	case "upload":
		res, err = scfm.Upload(ctx, source, target)
	case "get":
		res, err = scfm.Get(ctx, source, target)
	case "copy":
		res, err = scfm.Copy(ctx, source, target)
	case "delete":
		err = scfm.Delete(ctx, source)
	}
	if res != "" {
		if err != nil {
			fmt.Println("!!!", res)
		} else {
			fmt.Println(res)
		}
	}
}

// simple cloud file manager
type SCFM struct {
	Upload func(context.Context, string, string) (string, error)
	Get    func(context.Context, string, string) (string, error)
	Copy   func(context.Context, string, string) (string, error)
	Delete func(context.Context, string) error
}

func NewSCFM(provider, config, section string) (scfm *SCFM, err error) {
	var (
		oss *aliyun.OssClient
		s3  *aws.S3Client
	)

	scfm = new(SCFM)

	switch provider {
	case "oss":
		if section == "" {
			section = "aliyun_oss"
		}
		if oss, err = aliyun.NewOssClient(config, section); err != nil {
			return
		}

		scfm.Upload = func(ctx context.Context, source, target string) (string, error) {
			return oss.Upload(source, target)
		}

		scfm.Get = func(ctx context.Context, source, target string) (string, error) {
			return oss.Get(source, target)
		}

		scfm.Copy = func(ctx context.Context, source, target string) (string, error) {
			return oss.Copy(source, target)
		}

		scfm.Delete = func(ctx context.Context, source string) error {
			return oss.Delete(source)
		}
	case "s3":
		if section == "" {
			section = "aws_s3"
		}
		if s3, err = aws.NewS3Client(config, section); err != nil {
			return
		}

		scfm.Upload = s3.Upload

		scfm.Get = func(ctx context.Context, source, target string) (string, error) {
			return s3.Get(ctx, source, target)
		}

		scfm.Copy = func(ctx context.Context, source, target string) (string, error) {
			return s3.Copy(ctx, source, target)
		}

		scfm.Delete = func(ctx context.Context, source string) error {
			return s3.Delete(ctx, source)
		}
	default:
		return nil, fmt.Errorf("unkonwn cloud storage provider")
	}

	return
}
