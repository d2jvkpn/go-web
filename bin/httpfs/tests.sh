#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})

addr=http://localhost:3021

curl -i "$addr/static/healthz.txt"

curl -i "$addr/data/hello.txt"
