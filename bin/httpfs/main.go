package main

//go:generate bash go_build.sh

import (
	"context"
	"embed"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"syscall"
	"time"

	"github.com/d2jvkpn/go-web/pkg/misc"
	"github.com/d2jvkpn/go-web/pkg/wrap"
	"github.com/gin-gonic/gin"
)

var (
	//go:embed static
	_Static     embed.FS
	_NotWindows bool
)

func init() {
	misc.RegisterLogPrinter()
	_NotWindows = runtime.GOOS != "windows"
}

func main() {
	var (
		// release  bool
		addr     string
		dataDir  string
		err      error
		fileInfo fs.FileInfo
		fsys     fs.FS
		engi     *gin.Engine
		rg       *gin.RouterGroup
		server   *http.Server
	)

	meta := misc.BuildInfo()
	meta["project"] = "https://github.com/d2jvkpn/go-web/tree/dev/bin/httpfs"

	flag.StringVar(&addr, "addr", ":3021", "http serve address")
	flag.StringVar(&dataDir, "data", "./data", "data directory, url base path /data/")
	// flag.BoolVar(&release, "release", false, "run in release mode")

	flag.Usage = func() {
		output := flag.CommandLine.Output()

		fmt.Fprintf(
			output,
			"%s\n\nUsage of %s:\n",
			misc.BuildInfoText(meta),
			filepath.Base(os.Args[0]),
		)
		flag.PrintDefaults()
	}
	flag.Parse()

	gin.SetMode(gin.ReleaseMode)
	engi = gin.New()
	engi.Use(gin.Recovery())
	// engi = gin.Default()
	engi.Use(wrap.Cors("*"))
	rg = &engi.RouterGroup

	if fsys, err = fs.Sub(_Static, "static"); err != nil {
		log.Fatalln(err)
	}

	static := rg.Group("/static", wrap.CacheControl(3600))
	static.StaticFS("/", http.FS(fsys))

	if fileInfo, err = os.Stat(dataDir); err != nil {
		// os.IsNotExist(err)
		log.Fatalln(err)
	} else if !fileInfo.IsDir() {
		log.Fatalf("%s isn't a directory", dataDir)
	}

	wrap.ServeStatic("/data", dataDir, false)(rg)

	server = &http.Server{
		// ReadTimeout:       HTTP_ReadTimeout,
		// WriteTimeout:      HTTP_WriteTimeout,
		// ReadHeaderTimeout: HTTP_ReadHeaderTimeout,
		// MaxHeaderBytes:    HTTP_MaxHeaderBytes,
		Addr:    addr,
		Handler: engi,
	}

	errch, quit := make(chan error, 1), make(chan os.Signal, 1)

	shutdown := func() {
		var err error

		ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
		if err = server.Shutdown(ctx); err != nil {
			if _NotWindows {
				log.Printf("server shutdown: %v\n", err)
			}
		}
		cancel()
	}

	go func() {
		var err error

		log.Printf(">>> HTTP server listening on %s, Pid: %d\n", addr, os.Getpid())
		if err = server.ListenAndServe(); err != http.ErrServerClosed {
			shutdown()
		} else {
			err = nil
		}
		errch <- err
	}()

	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

	select {
	case err = <-errch:
	case <-quit:
		if _NotWindows {
			fmt.Println("")
		}
		shutdown()
		err = <-errch
	}

	if err != nil && _NotWindows {
		log.Fatalln(err)
	}
}
