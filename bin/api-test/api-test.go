package main

import (
	"fmt"
	"log"
	"os"

	"github.com/d2jvkpn/go-web/pkg/misc"
)

func main() {
	var (
		statusCode int
		fp         string
		body       string
		errStr     string
		err        error
		args       []string
		tmpls      []*misc.RequestTmpl
		rt         *misc.RequestTmpls
	)

	if len(os.Args) == 1 {
		fmt.Printf(
			"$ api-test <file.yaml> <api...>, file.yaml:\n``yaml\n%s```\n",
			misc.ExampleRequestTmpls(),
		)
		os.Exit(0)
	}

	fp, args = os.Args[1], os.Args[2:]

	if rt, err = misc.LoadRequestTmpls("api config", fp); err != nil {
		log.Fatalln(err)
	}

	if tmpls, err = rt.Match(args...); err != nil {
		log.Fatal(err)
	}

	errStr = "<nil>"
	for _, v := range tmpls {
		if statusCode, body, err = rt.Request(v); err != nil {
			errStr = err.Error()
		}

		fmt.Printf(
			">>> API: %s, Method: %s, Path: %s, StatusCode: %d, Error: %v\n%s\n\n",
			v.Name, v.Method, v.Path, statusCode, errStr, body,
		)

		if err != nil {
			os.Exit(1)
		}
	}
}
