#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})


host=http://localhost:3011

#### user register
curl -X POST $host/api/open/register -H "Host: dev.go-web.local" \
  -d '{"user":"rover","password":"aa123456"}'

#### user upload file(s)
curl -X PUT -u rover:aa123456 -H "Host: dev.go-web.local" \
  -H "Content-Type: multipart/form-data" \
  $host/api/auth/user/upload \
  -F "files=@./tests/user.sh"

### curl $host/uploads/rover/2022-08-29/user.1661766664227_9mH3v30yKRKHwFsF.sh

#### user unregister
curl -X POST -u rover:aa123456 -H "Host: dev.go-web.local" \
  $host/api/auth/user/unregister
