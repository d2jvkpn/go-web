#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})

# https://go.googlesource.com/proposal/+/master/design/45713-workspace.md

####
ls go-web

mkdir -p releases
cd releases && go mod init releases

####
cd ${_path}
go work init go-web releases
ls go.work go.work.sum

####
cd releases

go mod tidy

go build main.go
# go mod download, go mod graph, go mod verify, go mod why
# go mod edit, go mod init, go mod tidy, go mod vendor

./main
