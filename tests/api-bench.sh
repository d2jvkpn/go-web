#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})

host=http://localhost:3011

# plow -T "application/json" "$host/api/open/target" \
#   -d 30s --timeout 2s --body "@target.01.json" -c 1000

#### api hello
curl -X GET "$host/api/open/hello?id=8"

plow -m GET "$host/api/open/hello?id=8" -d 30s --timeout 2s -c 100

plow -m GET "$host/api/open/hello?id=8" -d 30s --timeout 2s -c 1000
