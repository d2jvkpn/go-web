package gjson_exp

import (
	"fmt"
	"strings"
	"testing"

	"github.com/tidwall/gjson"
)

var (
	_ans1 = `
{
    "Table": {
        "header": ["Brand", "Lighting", "Composition", "Location", "Detail", "Action", "Post-Processing"],
        "rows": [
            ["Ferrari F8 Tributo", "Soft natural light to accentuate curves", "Balanced position to highlight front grille and hood scoop", "On a winding road with picturesque scenery", "Leather interiors and carbon fiber accents", "Speeding around a track with a motion blur effect", "Professional color correction and touch-ups"],
            ["Porsche 911 GT3 RS", "Bright lighting to emphasize sharp body lines", "Positioned in a strong diagonal composition to draw the eye", "On a mountain road with sweeping landscapes", "Sport bucket seats and exposed roll cage", "Drifting around a corner with smoke effects", "Subtle editing to enhance tone and texture"],
            ["Lamborghini Aventador SVJ", "Harsh shadows to emphasize hard edges and angles", "Centered position to showcase signature scissor doors", "In front of a modern urban skyscraper", "Exposed engine bay with color contrasting lines", "Accelerating on a long, deserted highway", "Selective color and contrast adjustments for a bold and vivid look"],
            ["McLaren 720S", "Diffused light to bring out car's soft lines and curves", "Off-center composition to create visual interest", "In an industrial warehouse with rough metallic textures", "Twin-turbo V8 engine shown with open clamshell doors", "High-speed cornering on a scenic canyon road", "Retouching and color grading, with precise attention to color balance"],
            ["Aston Martin DB11", "Filtered light to create mood and atmosphere", "Angular position to accentuate the chiseled front end", "On a gravel road in a rural countryside setting", "Subtle touches of chrome and leather in the interior", "Roaring away from the camera with smoky tire effects", "Balanced adjustments to brightness, contrast, and hue"]
        ]
    }
}
`

	_ans2 = `
{
  "name": {"first": "Tom", "last": "Anderson"},
  "age":37,
  "children": ["Sara","Alex","Jack"],
  "fav.movie": "Deer Hunter",
  "friends": [
    {"first": "Dale", "last": "Murphy", "age": 44, "nets": ["ig", "fb", "tw"]},
    {"first": "Roger", "last": "Craig", "age": 68, "nets": ["fb", "tw"]},
    {"first": "Jane", "last": "Murphy", "age": 47, "nets": ["ig", "tw"]}
  ]
}
`
	_ans3 = `
{
  "data": [
    {
      "brand": "Ferrari",
      "model": "488 GTB",
      "lighting": "Natural sunlight",
      "composition": "Low angle shot with blurred background",
      "detail": "Close-up shot of the front grille and emblem",
      "action": "Captured in motion on a winding road",
      "cleanliness": "Spotless with no visible blemishes",
      "creativity": "Reflected in a puddle for a unique perspective"
    },
    {
      "brand": "Lamborghini",
      "model": "Huracan Evo",
      "lighting": "Studio lighting",
      "composition": "Symmetrical shot with a black background",
      "detail": "Detailed shot of the carbon fiber exterior",
      "action": "Captured drifting on a track",
      "cleanliness": "Meticulously cleaned with no smudges",
      "creativity": "Shot from above with a drone for an aerial view"
    },
    {
      "brand": "Porsche",
      "model": "911 GT3 RS",
      "lighting": "Sunset lighting",
      "composition": "Angular shot with a city skyline in the background",
      "detail": "Close-up shot of the rear wing and exhaust",
      "action": "Captured racing on a track",
      "cleanliness": "Clean with no noticeable dirt or dust",
      "creativity": "Shot through a tunnel for an interesting perspective"
    },
    {
      "brand": "McLaren",
      "model": "720S",
      "lighting": "Dusk lighting",
      "composition": "Diagonal shot with a mountain range in the background",
      "detail": "Detailed shot of the headlights and taillights",
      "action": "Captured speeding on an empty highway",
      "cleanliness": "Clean and polished with no visible scratches",
      "creativity": "Shot from a helicopter for a bird's eye view"
    },
    {
      "brand": "Aston Martin",
      "model": "DBS Superleggera",
      "lighting": "Indoor studio lighting",
      "composition": "Side profile shot with a white background",
      "detail": "Close-up shot of the wheels and brakes",
      "action": "Captured drifting on a wet surface",
      "cleanliness": "Spotless with no visible smudges or stains",
      "creativity": "Shot from inside the car for a unique perspective"
    }
  ]
}
`
)

func TestGjson(t *testing.T) {
	fmt.Println(">>> 1:", gjson.Get(_ans1, "Table.header").String())

	fmt.Println(">>> 2:", gjson.Get(_ans1, "Table.rows.0").String())

	values3 := make([]string, 0)
	for _, v := range gjson.Get(_ans1, "Table.rows.0").Array() {
		values3 = append(values3, v.String())
	}
	fmt.Println(">>> 3:", strings.Join(values3, ", "))

	fmt.Println(">>> 4:", gjson.Get(_ans2, "friends.0.@keys").Array())
	fmt.Println(">>> 5:", gjson.Get(_ans2, "friends.0.@values").Array())

	keys, values := make([]string, 0), make([]string, 0)
	gjson.Get(_ans2, "friends.0").ForEach(func(k, v gjson.Result) bool {
		keys = append(keys, k.String())
		values = append(values, v.String())
		return true
	})

	fmt.Printf(
		">>> 6:\n    keys: %s\n    values: %s\n",
		strings.Join(keys, ", "),
		strings.Join(values, ", "),
	)

	fmt.Printf(
		">>> 7:\n    %s\n    %t\n",
		gjson.Get(_ans3, "data.0.@values").String(),
		gjson.Get(_ans3, "Table.rows[0]").String() == "",
	)
}
