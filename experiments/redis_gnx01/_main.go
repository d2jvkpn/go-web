package main

import (
	"context"
	"fmt"
	"log"
	"time"

	redis "github.com/go-redis/redis/v9"
)

func main() {
	var (
		addr     string
		password string

		ctx    context.Context
		client *redis.Client
	)

	ctx = context.Background()
	addr, password = "127.0.0.1:6379", ""

	client = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password,
		DB:       0, // use default DB
	})

	ok, err := client.SetNX(ctx, "stocks", time.Now().UnixMilli(), 5*time.Second).Result()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("ok:", ok)
}

func NewMutex(client *redis.Client, key string, duration time.Duration) *Mutex {
	return &Mutex{client: client, key: key, duration: duration}
}

type Mutex struct {
	client   *redis.Client
	key      string
	value    int64
	duration time.Duration
}

func (item *Mutex) Lock(ctx context.Context) (bool, error) {
	item.value = time.Now().UnixMilli()
	return item.client.SetNX(ctx, item.key, item.value, item.duration).Result()
}

func (item *Mutex) Unlock(ctx context.Context) error {
	return item.client.Del(ctx, item.key).Err()
}
