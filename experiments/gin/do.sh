#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})

# go run gin_preprocess.go

addr=http://127.0.0.1:8000

curl -i -X GET $addr/hello_01
curl -i -X GET $addr/hello_01 -d '{}'
curl -i -X GET $addr/hello_01 -d '{"key":"name","value":"d2jvkpn"}'

curl -i -X GET $addr/hello_02
curl -i -X GET $addr/hello_02 -d '{}'
curl -i -X GET $addr/hello_02 -d '{"key":"name","value":"d2jvkpn"}'
