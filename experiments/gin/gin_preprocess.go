package main

import (
	"flag"
	// "fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	var (
		addr   string
		err    error
		engine *gin.Engine
		router *gin.RouterGroup
	)

	flag.StringVar(&addr, "addr", ":8000", "listening on address")
	flag.Parse()

	engine = gin.Default()
	// gin.SetMode(gin.ReleaseMode)
	// engine = gin.New()
	router = &engine.RouterGroup

	router.GET("/hello_01", middleware_01, hello_01)
	router.GET("/hello_02", middleware_02[Item](hello_02))

	if err = engine.Run(addr); err != nil {
		log.Fatalln(err)
	}
}

type Item struct {
	Key   string `json:"key,omitempty"`
	Value string `json:"value,omitempty"`
}

func middleware_01(ctx *gin.Context) {
	var (
		err  error
		item Item
	)

	if err = ctx.BindJSON(&item); err != nil {
		log.Println("!!! blocked by middleware_01")
		ctx.JSON(http.StatusBadRequest, gin.H{"code": -1, "msg": "failed to parse body"})
		ctx.Abort()
		return
	}

	ctx.Set("item", item)
	ctx.Next()
}

func hello_01(ctx *gin.Context) {
	intf, _ := ctx.Get("item")
	item, _ := intf.(Item)

	ctx.JSON(http.StatusOK, gin.H{"code": 0, "msg": "ok", "data": gin.H{"item": item}})
}

func middleware_02[B any](fn func(*gin.Context, B)) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var (
			err  error
			item B
		)

		if err = ctx.BindJSON(&item); err != nil {
			log.Println("!!! blocked by middleware_02")
			ctx.JSON(http.StatusBadRequest, gin.H{"code": -1, "msg": "failed to parse body"})
			ctx.Abort()
			return
		}

		fn(ctx, item)
	}
}

func hello_02(ctx *gin.Context, item Item) {
	ctx.JSON(http.StatusOK, gin.H{"code": 0, "msg": "ok", "data": gin.H{"item": item}})
}
