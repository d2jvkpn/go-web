package main

import (
	"fmt"
	"log"
	"time"

	"github.com/d2jvkpn/go-web/pkg/misc"
	"github.com/d2jvkpn/go-web/pkg/orm"
	"github.com/d2jvkpn/go-web/pkg/wrap"

	"github.com/jackc/pgconn"
	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/schema"
)

var (
	_DB *gorm.DB
)

func init() {
	misc.RegisterLogPrinter()
}

type User struct {
	ID        int64     `gorm:"column:id" json:"id,omitempty"`
	CreatedAt time.Time `gorm:"column:created_at;autoCreateTime" json:"createdAt,omitempty"`
	UpdatedAt time.Time `gorm:"column:updated_at;autoUpdateTime" json:"updatedAt,omitempty"`
	Status    string    `gorm:"column:status;type;default:'ok'" json:"status,omitempty"`

	Name         string          `gorm:"column:name" json:"name,omitempty"`
	Email        string          `gorm:"column:email" json:"email,omitempty"`
	Phone        string          `gorm:"column:phone" json:"phone,omitempty"`
	Birthday     string          `gorm:"column:birthday" json:"birthday,omitempty"`
	Attributions orm.Map[string] `gorm:"column:attributions;type:json" json:"attributions,omitempty"`
}

func main() {
	var (
		err    error
		vc     *viper.Viper
		result *gorm.DB
	)

	if vc, err = wrap.OpenConfig("a01_connect.auth", "yaml"); err != nil {
		return
	}

	conf := &gorm.Config{
		NamingStrategy: schema.NamingStrategy{SingularTable: true},
	}
	// dsn: "host=%s user=%s password=%s dbname=%s port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	if _DB, err = gorm.Open(postgres.Open(vc.GetString("dsn")), conf); err != nil {
		log.Fatalln(err)
		return
	}
	_DB = _DB.Debug()

	jane := User{
		Name: "Jone", Email: "jone@gmail.com", Birthday: "2000-01-01",
		Attributions: map[string]string{"gender": "female"},
	}

	if err = _DB.Table("users").Create(&jane).Error; err != nil {
		if e, ok := err.(*pgconn.PgError); ok && e.Code == "23505" {
			fmt.Println("!!! user already exists:", e.ConstraintName)
			err = nil
		} else {
			log.Fatalln(err)
			return
		}
	}

	//
	doe := User{}
	result = _DB.Table("users").Where("id = 101").Limit(1).Find(&doe)
	if err = result.Error; err != nil {
		log.Fatalln(err)
		return
	} else if result.RowsAffected == 0 {
		fmt.Println("!!! not found")
	} else {
		fmt.Println("~~~", doe)
	}

	result = _DB.Table("users").Where("id = 101").First(&doe)
	if err = result.Error; err != nil {
		if err.Error() == "record not found" {
			fmt.Println("!!! not found")
		} else {
			log.Fatalln(err)
			return
		}
	} else {
		fmt.Println("~~~", doe)
	}

	///
	users := []User{}
	if err = _DB.Table("users").Find(&users).Error; err != nil {
		log.Fatalln(err)
		return
	}
	fmt.Printf("~~~ %#v\n", users)

	///
	items := []User{}
	// clause.Returning{Columns: []clause.Column{{Name: "id"}, {Name: "name"}, {Name: "attributions"}}}
	// update users set status = 'blocked' where status = 'ok' returning *;
	if err := _DB.Table("users").Model(&items).Clauses(clause.Returning{}).
		Where("status = ?", "ok").Update("status", "blocked").Error; err != nil {
		log.Fatalln(err)
		return
	}
	fmt.Printf("~~~ %v\n", items)
}
