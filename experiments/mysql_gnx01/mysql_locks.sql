use gnx01;

---- pessimistic locking
select @@autocommit;
set autocommit = 0;
select @@autocommit;

select * from stocks where id = 1;

select * from stocks where id = 1 for update; -- lock

update stocks set stock = stock - 1 where id = 1;

commit;

---- optimistic locking
set autocommit = 1;

update stocks set stock = stock - 40 where id = 1 and stock > 40;

---- transaction
START TRANSACTION;

SELECT @A:=SUM(salary) FROM table1 WHERE type=1;
UPDATE table2 SET summary=@A WHERE type=1;

COMMIT;
