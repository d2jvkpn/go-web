CREATE DATABASE IF NOT EXISTS gnx01 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;

USE gnx01;

----
CREATE TABLE IF NOT EXISTS goods (
	id           bigint     NOT NULL AUTO_INCREMENT,
	created_at   timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP,
	created_date char(12) GENERATED ALWAYS AS (DATE(created_at)) STORED,
	updated_at   timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	status       enum('created', 'ok', 'canceled')  NOT NULL DEFAULT 'created',

	name   varchar(30)  NOT NULL,
	stock  int          NOT NULL DEFAULT 0,
	tags   json,

	PRIMARY  KEY (id),
	KEY      created_date_desc__status (created_date DESC, status),
	KEY      updated_at_desc (updated_at DESC),
	KEY      item (name, stock)
) ENGINE=InnoDB COMMENT='goods table';

----
CREATE USER IF NOT EXISTS 'd2jvkpn'@'localhost' IDENTIFIED BY 'password';
-- DROP USER 'd2jvkpn'@'localhost';
CREATE USER IF NOT EXISTS 'd2jvkpn'@'%' IDENTIFIED BY 'password';
-- ALTER USER 'd2jvkpn'@'%' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON gnx01.* TO 'd2jvkpn'@'localhost';
GRANT ALL PRIVILEGES ON gnx01.* TO 'd2jvkpn'@'%'; -- not secure
FLUSH PRIVILEGES;
