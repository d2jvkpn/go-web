#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})

docker exec -i mysql_gnx01 sh -c 'exec mysql -u root -pd2jvkpn' < init.sql
