#! /usr/bin/env bash
set -eu -o pipefail
_wd=$(pwd)
_path=$(dirname $0 | xargs -i readlink -f {})

# go run main.go

addr=http://localhost:8081

####
curl -i -X GET "$addr/goods/find?id=1"
# stock: 42
curl -i -X POST "$addr/goods/create" -d "@goods01.json"

####
plow -m POST "$addr/goods/reduce_v1?id=1&num=10" -c 5 -n 10

plow -m POST "$addr/goods/reduce_v2?id=1&num=10" -c 5 -n 10

curl -i -X GET "$addr/goods/find?id=1"
# stock 2
