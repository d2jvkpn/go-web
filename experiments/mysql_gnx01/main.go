package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/d2jvkpn/go-web/pkg/orm"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	_MYSQL_DSN = "d2jvkpn:password@tcp(localhost:33061)/gnx01?charset=utf8mb4&parseTime=true&loc=Local"
)

var (
	_ReduceGoodsStock *sync.Mutex
	_DB               *gorm.DB
)

func init() {
	_ReduceGoodsStock = new(sync.Mutex)
}

func main() {
	var (
		release bool
		addr    string
		err     error
		engi    *gin.Engine
		router  *gin.RouterGroup
	)

	flag.StringVar(&addr, "addr", ":8081", "http listening address")
	flag.BoolVar(&release, "release", false, "run in release mode")
	flag.Parse()

	if _DB, err = orm.ConnectDSN(_MYSQL_DSN, true); err != nil {
		log.Fatal(err)
	}

	if release {
		gin.SetMode(gin.ReleaseMode)
		engi = gin.New()
		engi.Use(gin.Recovery())
	} else {
		engi = gin.Default()
	}
	router = &engi.RouterGroup

	///
	if err = _DB.Table("goods").Where("id = 1").Update("stock", 42).Error; err != nil {
		log.Fatal(err)
	}

	///
	router.GET("/hello", Hello)

	goods := router.Group("/goods")
	goods.GET("/find", func(ctx *gin.Context) {
		var (
			id    uint64
			err   error
			goods *Goods
		)

		id, _ = strconv.ParseUint(ctx.DefaultQuery("id", ""), 10, 64)
		if goods, err = FindGoods(ctx, id); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"code": -1, "data": gin.H{}})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"code": 0, "data": gin.H{"item": goods}})
	})

	goods.POST("/reduce_v1", func(ctx *gin.Context) {
		var (
			id, num uint64
			code    int
			err     error
		)

		id, _ = strconv.ParseUint(ctx.DefaultQuery("id", ""), 10, 64)
		num, _ = strconv.ParseUint(ctx.DefaultQuery("num", ""), 10, 64)

		if code, err = ReduceGoodsStockV1(ctx, id, num); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"code": code, "data": gin.H{}})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{"code": 0, "data": gin.H{}})
	})

	goods.POST("/reduce_v2", func(ctx *gin.Context) {
		var (
			id, num uint64
			code    int
			err     error
		)

		id, _ = strconv.ParseUint(ctx.DefaultQuery("id", ""), 10, 64)
		num, _ = strconv.ParseUint(ctx.DefaultQuery("num", ""), 10, 64)

		if code, err = ReduceGoodsStockV2(ctx, id, num); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"code": code, "data": gin.H{}})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{"code": 0, "data": gin.H{}})
	})

	goods.POST("/reduce_v3", func(ctx *gin.Context) {
		var (
			id, num uint64
			code    int
			err     error
		)

		id, _ = strconv.ParseUint(ctx.DefaultQuery("id", ""), 10, 64)
		num, _ = strconv.ParseUint(ctx.DefaultQuery("num", ""), 10, 64)

		if code, err = ReduceGoodsStockV3(ctx, id, num); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"code": code, "data": gin.H{}})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{"code": 0, "data": gin.H{}})
	})

	goods.POST("/create", func(ctx *gin.Context) {
		var (
			err   error
			goods Goods
		)
		if err = ctx.BindJSON(&goods); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"code": -1, "data": gin.H{}})
			return
		}

		if _, err = CreateGoods(ctx, &goods); err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{"code": 1, "data": gin.H{}})
			return
		}
		ctx.JSON(http.StatusOK, gin.H{"code": 0, "data": gin.H{"item": goods}})
	})

	engi.Run(addr)
}

func Hello(ctx *gin.Context) {
	ctx.String(200, "Hello, world!\n")
}

// / biz
type Goods struct {
	Id        uint64    `gorm:"column:id;primary_key" json:"id,omitempty"`
	CreatedAt time.Time `gorm:"column:created_at;autoCreateTime" json:"createdAt,omitempty"`
	UpdatedAt time.Time `gorm:"column:updated_at;autoUpdateTime" json:"updatedAt,omitempty"`
	Status    string    `gorm:"column:status;default:created" json:"status,omitempty"`

	Name  string             `gorm:"column:name" json:"name,omitempty"`
	Stock int64              `gorm:"column:stock" json:"stock,omitempty"`
	Tags  orm.Vector[string] `gorm:"column:tags" json:"tags,omitempty"`
}

func FindGoods(ctx *gin.Context, id uint64) (goods *Goods, err error) {
	if id == 0 {
		return nil, fmt.Errorf("invalid id")
	}

	goods = new(Goods)
	err = _DB.WithContext(ctx).Table("goods").Where("id = ?", id).First(goods).Error
	if err != nil {
		return nil, err
	}

	return goods, nil
}

func ReduceGoodsStockV1(ctx context.Context, id, num uint64) (code int, err error) {
	if id == 0 || num == 0 {
		return -1, fmt.Errorf("invalid parameter")
	}

	goods := Goods{}
	tx := _DB.Begin().WithContext(ctx)
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	// https://gorm.io/docs/advanced_query.html
	err = tx.Clauses(clause.Locking{Strength: "UPDATE"}).
		Table("goods").Where("id = ?", id).Find(&goods).Error
	/*
		-- field after where must indexed, or row lock will become table lock
		select @@autocommit;
		set autocommit = 0;

		begin;
		select * from goods where id = 1 for update;
		update set stocks = 99 goods where id = 1;
		-- ?? rollback;
		commit;

		set autocommit = 1;
	*/
	if err != nil {
		return 1, err
	}

	if goods.Stock -= int64(num); goods.Stock < 0 {
		err = fmt.Errorf("no stock")
		return 2, err
	}

	err = tx.Table("goods").Where("id = ?", id).Update("stock", goods.Stock).Error
	if err != nil {
		return 3, err
	}

	return 0, nil
}

func ReduceGoodsStockV2(ctx context.Context, id, num uint64) (code int, err error) {
	if id == 0 || num == 0 {
		return -1, fmt.Errorf("invalid parameter")
	}

	tx := _DB.WithContext(ctx).Table("goods")

	result := tx.Where("id = ? AND stock >= ?", id, num).Update("stock", gorm.Expr("stock -?", num))
	if err = result.Error; err != nil {
		return 1, err
	}
	if result.RowsAffected == 0 {
		return 2, fmt.Errorf("no stock")
	}

	return 0, nil
}

// _ReduceGoodsStock doesn't work in different instances(containers)
func ReduceGoodsStockV3(ctx context.Context, id, num uint64) (code int, err error) {
	if id == 0 || num == 0 {
		return -1, fmt.Errorf("invalid parameter")
	}

	_ReduceGoodsStock.Lock()
	tx := _DB.WithContext(ctx).Begin().Table("goods")

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
		_ReduceGoodsStock.Unlock()
	}()

	result := tx.Where("id = ? AND stock >= ?", id, num).Update("stock", gorm.Expr("stock -?", num))
	if err = result.Error; err != nil {
		return 1, err
	}
	if result.RowsAffected == 0 {
		return 2, fmt.Errorf("no stock")
	}

	// ... more operations

	return 0, nil
}

func CreateGoods(ctx context.Context, goods *Goods) (id uint64, err error) {
	var at time.Time
	goods.Id, goods.Status = 0, "created"
	goods.CreatedAt, goods.UpdatedAt = at, at

	if err = _DB.WithContext(ctx).Table("goods").Create(goods).Error; err != nil {
		return 0, err
	}

	return goods.Id, nil
}
