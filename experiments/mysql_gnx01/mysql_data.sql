USE gnx01;

----
-- INSERT IGNORE INTO goods (id, name, stock) VALUES
--   (1, 'Apple', 42), (2, 'Banana', 0), (3, 'Cat', 24);

INSERT IGNORE INTO goods (id, name, stock) VALUES
  (1, 'Apple', 42), (2, 'Banana', 42), (3, 'Cat', 42)
  ON DUPLICATE KEY UPDATE stock=42;
-- Query OK, 6 rows affected (0.04 sec)
-- Records: 3  Duplicates: 0  Warnings: 0


SELECT * FROM goods \G;
EXPLAIN SELECT * FROM goods WHERE id > 1; -- use primary key id

EXPLAIN SELECT * FROM goods WHERE stock > 0; -- can't use key item
EXPLAIN SELECT * FROM goods WHERE name = 'Apple'; -- use key item
EXPLAIN SELECT * FROM goods WHERE name = 'Apple' AND stock = 1; -- use key item
